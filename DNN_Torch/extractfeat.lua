print '==> defining extraction procedure'
-- Log results to files
-- Returns the filename ( removes the extension ), does not work with abs path!
function getfilename(filepath)
  return filepath:match("[^/]+$")
end

-- extractfeats function
function extractfeats(inputs,outputlayer)
   -- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
   model:evaluate()

   -- if opt.type == 'double' then inputs = inputs:double()
   if opt.type == 'cuda' then
       inputs = inputs:cuda()
   end

   local preds = model:forward(inputs)
   -- Take the output of the layer before the last one
   local botneckout = model.modules[outputlayer].output:float()

   -- local botnecktable = torch.totable(botneckout)

   local n_samples=botneckout:size(1)
   local dim = botneckout:size(2)
   return botneckout,n_samples,dim


end
