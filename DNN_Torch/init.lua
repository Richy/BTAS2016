require 'torch'   -- torch
require 'os'   --
require 'nn'      -- provides a normalization operator
-- require 'cunn'
require 'xlua'    -- xlua provides useful tools, like progress bars
require 'optim'   -- an optimization package, for online and batch methods
require "libhtktoth"
require 'nngraph'

require 'libpreparedata'
-- Add the wavelet path to the current package path
-- package.path = package.path .. ";wavelet/?.lua"
-- require ("waveletpooling")

-- This file parses the command line args
-- And sets the global variable model
-- Model is the current model for the NN

if not (opt) then
    cmd = torch.CmdLine()
    cmd:text()
    cmd:text('Speaker Verification with DNN')
    cmd:text()
    cmd:text('Options:')
    -- filelist:
    cmd:option('-featfile', '', 'name a file storing all the filenames of data')
    -- The input data file, please make sure that it is already shuffleded in advance since we do not shuffle the whole dataset, but only a small subset
    -- Of the provided files at a time
    cmd:option('-scpfile', '', 'name a file storing all the filenames of train or test data')
    cmd:option('-cachesize',16384,'nb of samples which are loaded in at once')
    cmd:option('-labelfile', '', 'name a file storing the labels for each file in scp')
    cmd:option('-cvscpfile', '', 'name a file storing all the filenames of cv data')
    cmd:option('-globalnorm', '', 'normalization file contains the means and variances')
    cmd:option('-testoutput','test_results','Default output for the test values when running test.lua')
    -- global:
    cmd:option('-seed', 1, 'fixed input seed for repeatable experiments')
    cmd:option('-threads', 4, 'number of threads')
    -- data:
    cmd:option('-size', 'full', 'how many samples do we load: small | full | extra')
    -- model:
    cmd:option('-model', 'jvectorELU', 'type of model to construct: jvectorELU | jvectorPReLU | convnet')
    cmd:option('-ldmodel', 'model.net', 'name of the model to be loaded')
    cmd:option('-nfeats',3,'number of features ( delta, delta delta ). Only for CNN')
    cmd:option('-noutputs',0,'nb of output neurons (aka speakers)')
    cmd:option('-nouttypes',0,'nb of output neurons for the attack types')
    cmd:option('-inputdim',0,'nb of the single input')
    cmd:option('-cudnn',false,"Enables CUDNN")
    -- For specifying multiple output layers, we need to put this as a string per default
    cmd:option('-outlayer',"",'When extracting features, the effective number of layer the output is taken from. If this parameter is not modified, we use the last layer. Multiple layers are also possible and need to be specified as -output \"3 1 2\".')
    cmd:option('-fext',5,'nb of frames which will be extended')
    cmd:option('-layers',"",'Hidden layer dimensions. Specify using a separated " " with whitespace separation. e.g. -layers "1024 1024"')
    cmd:option('-filtsizew',11,'filter size width of the filter. Only for CNN')
    cmd:option('-filtsizeh',3,'filter size height of the filter. Only for CNN')
    cmd:option('-dfiltsizew',1,'the step of the convolution in width dimension. Only for CNN')
    cmd:option('-dfiltsizeh',1,'the step of the convolution in height dimension. Only for CNN')
    cmd:option('-poolsizew',2,'the kernel width of the pooling. Only for CNN')
    cmd:option('-poolsizeh',2,'the kernel height of the pooling. Only for CNN')
    cmd:option('-dpoolsizew',2,'the step of the pooling in width dimension. Only for CNN')
    cmd:option('-dpoolsizeh',2,'the step of the pooling in height dimension. Only for CNN')
    cmd:option('-poolMethod','MaxP','type of the pooling method to construct: MaxP | LPP | wavelet. Only for CNN')
    cmd:option('-pnorm','2','norm of LPNorm Pooling in LPPooling. Only for CNN')
    -- loss:
    cmd:option('-loss', 'nll', 'type of loss function to minimize: nll | mse | margin')
    -- training:
    cmd:option('-save', 'results', 'subdirectory to save/log experiments in')
    cmd:option('-plot', false, 'live plot')
    cmd:option('-optimization', 'adadelta', 'optimization method: SGD | ASGD | CG | LBFGS | adagrad |adadelta')
    cmd:option('-learningRate', 0.3, 'learning rate at t=0')
    cmd:option('-batchSize', 64, 'mini-batch size (1 = pure stochastic)')
    cmd:option('-weightDecay', 0, 'weight decay (SGD only)')
    cmd:option('-momentum', 0.8, 'momentum (SGD only)')
    cmd:option('-t0', 1, 'start averaging at t0 (ASGD only), in nb of epochs')
    cmd:option('-maxIter', 2 , 'maximum nb of iterations for CG and LBFGS')
    cmd:option('-minIter',20,'minimum amount of iterations for the NN')
    -- cmd:option('-l1',0,'L1 regularization coefficient while training')
    -- cmd:option('-l2',0,'L2 regularization coefficient while training')
    cmd:option('-type', 'float', 'type: double | float | cuda')
    --test
    cmd:option('-featuredir','feature','Name of the directory where the saved features are going to be stored')
    cmd:text()
    opt = cmd:parse(arg or {})
end

-- Utility function to split a string by a blank separator, used for -layers argument
function split(str)
    ret = {}
    for i in string.gmatch(str, "%d+") do
        ret[#ret+1] = tonumber(i)
    end
    return ret
end

print '==> processing options'
-- nb of threads and fixed seed (for repeatable experiments)
if opt.type == 'float' then
    print('==> switching to floats')
    torch.setdefaulttensortype('torch.FloatTensor')
elseif opt.type =='double' then
    print("==> switching to doubles")
    torch.setdefaulttensortype('torch.DoubleTensor')
elseif opt.type == 'cuda' then
    print('==> switching to CUDA')
    require 'cunn'
    require 'cutorch'
    if opt.cudnn then
        require 'cudnn'
        print("==> Switching to CuDNN")
    end
    cutorch.setHeapTracking(true)
    --It is somehow a bad practice ( still  ) to use CudaTensor as default type since not all API calls are supported (especially map2)
    local devicecount = cutorch.getDeviceCount()
    if devicecount > 1 then
        local maxfree = -1
        local maxid = 1
        for i=1,devicecount do
            free,total = cutorch.getMemoryUsage(i)
            if free > maxfree then
                maxfree = free
                maxid = i
            end
        end
        cutorch.setDevice(maxid)
        print('==> Using device',maxid,"because it still has ",maxfree,"kb free memory")
    end
    torch.setdefaulttensortype('torch.FloatTensor')
end
torch.setnumthreads(opt.threads)
torch.manualSeed(opt.seed)

assert(opt.cachesize > 2^11, "Cachesize cant be smaller than 2048")
assert(opt.cachesize > opt.batchSize,"Cachesize needs to be larger than the batchsize")
-- needs to be required here, since otherwise the data and model functions are not initzialized with the same tensor
---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
require 'data'
require 'batch'
require 'model'
require 'loss'
---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------


print '==> define parameters'
-- hidden units (for creating new model or loading model from binary)
-- local nstates = {1024,1024,1024}
filtsizew = opt.filtsizew
filtsizeh = opt.filtsizeh
dfiltsizew = opt.dfiltsizew
dfiltsizeh = opt.dfiltsizeh
poolsizew = opt.poolsizew
poolsizeh = opt.poolsizeh
dpoolsizew = opt.dpoolsizew
dpoolsizeh = opt.dpoolsizeh

-- number of units in output layer, but meaningless in loading model from binary file
local noutputs = opt.noutputs
-- [Number of incorelated features], [Width and Height for each feature map(height is the extended frame)], [Number of units in input layer] (for creating new model only)

-- nfeats = 3
--nfeats is the number of dynamic elements for every feature ( usually3 )
nfeats = opt.nfeats
-- We assume having dynamic feats as input
width = opt.inputdim
if opt.model:starts('convnet') then
    width = opt.inputdim/nfeats
end
height = 2*opt.fext+1
ninputs = width*height
-- number of hidden units (for MLP only):
nhiddens = ninputs / 2

-- Loading the model!
local filename = paths.concat(opt.save, opt.ldmodel)
model = io.open(filename, 'rb')
if (model)then
    model=loadmodelfromfile(filename)
    -- Count the current amount of linear nodes
    linear_nodes = model:findModules('nn.Linear')
    -- First and last linear nodes are not hidden layers
    nhidden_layers = #linear_nodes - 2

    -- current hidden layer
    local curhid = (model:size()-#linear_nodes)/nhidden_layers+1  -- 5 is the number of input and output layers (Lin, Re, Lin, LogSM)
    -- If we changed the shape of the current model!
    nstates = split(opt.layers)
    if (opt.model=='deepneunet' and curhid<opt.hidlaynb and opt.hidlaynb~=0 and opt.hidlaynb<=#nstates) then
        assert(#nstates > 0," Number of given hidden layers is "..#nstates.. ". Please define your hidden layers with -layers \"1024 1024 1024\" ")
        model = extendModel(model,nstates)
    end
    -- Retrieve the default values for the network model
    opt.fext = model.fext
    opt.noutputs = model.noutputs
    opt.filtsizeh = model.filtsizeh
    opt.filtsizew = model.filtsizew

    width = model.width
    nfeats = model.nfeats
    height = model.height
    ninputs = width * height
    -- restore the model type
    opt.model = model.modeltype


else
    -- Check if some arguments which are necessary are given
    assert(opt.noutputs ~= 0,"Please define a number of outputs with -noutputs")
    assert(opt.inputdim ~= 0,"Please define the (static) dimension of inputs with -inputdim")
    nstates = split(opt.layers)
    assert(#nstates > 0," Number of given hidden layers is "..#nstates.. ". Please define your hidden layers with -layers \"1024 1024 1024\" ")
    newmodel(nstates,ninputs,noutputs,opt.fext)
end
printmodel()
if opt.type =='cuda' then
    if opt.cudnn then
        print("=> Converting model to CUDNN")
        cudnn.convert(model,cudnn)
    end
    model= model:cuda()
    criterion=criterion:cuda()
end


