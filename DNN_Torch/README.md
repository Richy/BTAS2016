# Training a DNN for speaker verification with torch

## Models

This framework provides many different models to do speaker verification tasks. Most commonly is the general D-Vector approach, where a feature vector is extracted from a hidden layer.

Features need to be in HTK format to work properly.

- D-Vector
- J-Vector ( A multi-task D-Vector approach )
- CNN based D-Vectors
- Wavelet based CNN

## Installation

To install please simply run:

```bash
git clone https://gitlab.com/simpleoier/DNNSV.git
cd DNNSV
git submodule --recursive
cd htklua
luarocks make
```


## Usage

### Iterators

We use a self written Cache/ Batch Iterator to iterate over the given dataset. These iterators provide a fast and relyable API and only need besides the data files a function to extract the label from a given filename.
e.g. assuming we have a label, in the labelfile defined as:
```
Speaker1 1
```

And the corresponding data file looks like:

```
/path/to/Speaker1_Utt1.feat
```

Then in our code, we define the function getFilelabel to extract from the datafile, the speaker label. The input for this function is the current line of the datafile e.g. /path/to/Speaker1_Utt1.feat.

The filelabels need to be processed in advance to obtain a table of all labels.
```lua
function getfilelabel(line)
	local spkid = line:split("_")[1]
	return filelabels[spkid]
end
```

### D-vector

For D-Vector we need the following files:

Datainputfile:
The basic data file has a simple format:
```
FeatureFile1
FeatureFile2
...
```
Note that every feature needs currently to be in HTK format.

Moreover the label file is defined as:
```
SpkidLabel1 1
SpkidLabel2 2
...
```

Now after these files are set, we only need to define how many neurons and layers our model has. This can be easily done by simply inputting the network size into the console as:

```bash
th trainmain.lua -scpfile MYDATAFILE -labelfile MYLABELFILE -noutputs NUMBEROFOUTNEURONS -layers "1024 1024 1024" #3 hidden layers with 1024 neurons each
```

### J-Vector

The J-Vector works entirely as the D-Vector, but in addition also adds Utterance information.
If we have Utterances present in our dataset, e.g. file labels are speaker1_channel1_utter1.feat, then we only need to adjust the label file to use j-vector.

The labels are then composed of two differnt sets, which we both fuse into one label file. If the speaker labels are as follows:
```
Speaker1 1
Speaker2 2
...
SpeakerN N
```

And we have in addition M utterances, we can produce the following file:
```
Utterance1 1
Utterance2 2
...
UtteranceM M
```

For J-vector we then simply fuse these two files into one:
```bash
cat speakerlabels utterancelabels > alllabels
```
And input this alllabels file as standard input. Not that to distinguish between speaker and utterances, we need to additionally add the -noututts parameter to specify how many utterances are present.



### CNN

CNN works entirely the same as D-vector. The only main difference is that the Pooling methods and the filtersizes of the pools can be adjusted.


## Testing/Feat extract
To do the Feature extraction, we provide several convenience methods, such as specifying the output layer. Testing with direct LogSoftMax output is also possible, but generally will not perform well.

```bash
th testmain.lua -scpfile TESTDATA -save PATH/TO/MODEL
```
To extract features from a given layer, please use:
th extractfeatmain.lua -scpfile TESTDATA -save PATH/TO/MODEL -outlayer OUTPUTLAYER
```
