print '==> executing all'

-- Also includes data and model
require 'init'
require 'extractfeat'

----------------------------------------------------------------------
print("==> Extracting features")

if not opt.scpfile then
    error("Please specify a file containing the data with -scpfile")
    return
elseif io.open(opt.scpfile,"rb") == nil then
    error(string.format("Given scp file %s cannot be found!",opt.scpfile))
    return
end

-- Define the output path for the features
local modelname=getfilename(opt.ldmodel).. "_" .. opt.featuredir
local outputpath = paths.concat(opt.save,modelname)
os.execute('mkdir -p ' .. outputpath)

-- Default output layer is the last one
local outputlayer = {#model.modules-2}

if opt.outlayer then
    local outputlayers=split(opt.outlayer)

    -- Only having a single value in the table, thus just use this value
    if #outputlayers == 1 then
        local out = tonumber(outputlayers[1])
        if out == 0 then
            out = #model.modules-2
        end
        outputlayer = {out}

    -- When having multiple variables, we set outputlayer as a table of these layers
    else
        outputlayer = outputlayers
    end

end

print(sys.COLORS.red.."==> Saving output layer "..table.concat(outputlayer,",").." to " .. outputpath)

-- For the usual case our inputs are one dimensional
local datadim = { ninputs }
if (opt.model:starts('convnet')) then
    datadim = { nfeats, height, width }
end


local testcache = FileCache(opt.scpfile,opt.cachesize,false,opt.fext)


local testiterator = Chunkiterator(testcache,false,opt.batchSize,datadim,mean,var)

-- We do not need any labels for extracting the features
local testbatchiter = testiterator:iterator(function(line) return 1 end)
local batchnum = 0
local starttime = torch.tic()

for inputs,targets,filename in testbatchiter do
    batchnum = batchnum + 1
    xlua.progress(batchnum,testcache._uttsize)

    -- Accumulators for dimensions and all features
    local tot_samples = 0
    local tot_dim = 0
    local tot_feature = {}
    -- Run over all the output layers
    for k,outl in pairs(outputlayer) do

        -- A problem might occur here, if the given utterance is very long (1000+ frames), thus not being able to copy to the GPU
        -- Batchsize is essential, since otherwise we maybe overflow the GPU RAM when copying the whole utterance onto the GPU
        local splits = inputs:split(opt.batchSize)
        local dim = 0
        for k,inputsplit in pairs(splits) do
            feat,n_samples,dim = extractfeats(inputsplit,outl)
            -- Accumulate the features ( if there are multiple)
            table.insert(tot_feature,feat)
            tot_samples = tot_samples + n_samples
        end
        -- Increase dimensionality for each outputlayer
        tot_dim = tot_dim + dim
    end
    collectgarbage()
    -- Concatenate the features into one large table
    catfeat = torch.cat(tot_feature,1)

    botneck1d_feat = torch.totable(catfeat:view(catfeat:nElement()))

    local outputfeature = paths.concat(outputpath,filename)
    writehtk(outputfeature,tot_samples,100000,tot_dim,"USER",botneck1d_feat)
end
print("==> Extraction time was "..torch.toc(starttime) .. " s")

if (opt.crossvalid==1) then
    confusion:updateValids()
    print('average row correct: ' .. (confusion.averageValid*100) .. '%')
    print('average rowUcol correct (VOC measure): ' .. (confusion.averageUnionValid*100) .. '%')
    print('global correct: ' .. (confusion.totalValid*100) .. '%')
end
