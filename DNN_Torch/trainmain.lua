#!/usr/bin/env th
print '==> executing all'
-- Extend the path so that this script can be used from other folders
-- Parses the input arguments and sets the variables for the model and certain other variables such as:
-- model : The Neural network model
-- opt : The parsed arguments from the command line
require 'init'
require 'train'
----------------------------------------------------------------------

-- Asserts for necessary data files
assert(opt.scpfile,"Could not find scpfile. Specify with -scpfile")
assert(opt.labelfile,"Could not find labelfile. Specify with -labelfile")

function trainmain(batch)
	local batchnum = 0
	local trainerr = 0
	local trainiter = batch:iterator(getfilelabel)
	local traintime = torch.tic()
	-- training
	confusionTrain:zero()
	-- Set model to training mode
	model:training()
	for inputs,targets in trainiter do
		if opt.type == 'cuda' then
			inputs = inputs:cuda()
			targets = targets:cuda()
		end
		batchnum = batchnum + 1
		xlua.progress(batchnum,math.ceil(batch:size()/batch.batchsize))
		trainerr = trainerr + trainbatch(inputs,targets)
		-- -- Zero the statistics
	end
	-- training finished
	traintime = torch.toc(traintime)
	print("==> time to train 1 batch = " .. (traintime/batch:size()) .. 'ms')
	confusionTrain:updateValids()
	print(sys.COLORS.cyan.."\nIteration #"..trainIter .." done ! Average correct: "..string.format("%.5f%%",confusionTrain.averageValid*100)..'\n')
	trainerr = trainerr/batchnum
	local trainacc = confusionTrain.totalValid*100
	-- Meansure time for training
	return traintime,trainerr,trainacc
end



function cvmain(cvbatch)

	local batchnum = 0
	local testerr = 0
	local cviter = cvbatch:iterator(getfilelabel)
	confusionCV:zero()
    model:evaluate()

	-- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
	local testtime =torch.tic()
	for inputs,targets in cviter do
		if opt.type == 'cuda' then
			inputs = inputs:cuda()
			targets = targets:cuda()
		end
		batchnum = batchnum + 1
		xlua.progress(batchnum,math.ceil(cvbatch:size()/cvbatch.batchsize))
		testerr = testerr + crossvalidatebatch(inputs,targets)
	end
	testtime = torch.toc(testtime)
	print("==> time to test 1 batch = " .. (testtime/cvbatch:size()) .. 'ms')
	confusionCV:updateValids()
	print(sys.COLORS.yellow .. "==> Crossvalidation #"..trainIter.." done ! Average correct "..string.format("%.5f%%",confusionCV.averageValid*100) ..'\n')

	testerr = testerr/batchnum
	local testacc = confusionCV.totalValid*100

	return testtime,testerr,testacc

end

-- Iterator count
trainIter = 0
local traintime,trainerr,trainacc,cvtime,cverr,cvacc
local bestacc=0
-- acclog = assert(io.open(paths.concat(opt.save, 'acc_'..opt.ldmodel..'.log'),'w'))
statslog = optim.Logger(paths.concat(opt.save, 'train_'..opt.ldmodel..'.log'),false)
accLogger = optim.Logger(paths.concat(opt.save,'acc_'..opt.ldmodel..'.log'),false)
errLogger= optim.Logger(paths.concat(opt.save,'err_'..opt.ldmodel..'.log'),false)

statslog:setNames{"Iter","Time","TAcc","RowAcc","VOC","Error","Type"}

-- We need to specify the scp file, no need to check. We did already one check in init.lua
print('==> Preparing data/normalizing')

-- For the usual case our inputs are one dimensional
local datadim = { ninputs }
if (opt.model:starts('convnet')) then
	datadim = { nfeats , height, width }
end

-- reading labels from label file
local filelabels=readLabel(opt.labelfile)

-- function to get the label from the filename
function getfilelabel(line)
	local fname = line:split("%.")[1]
	return filelabels[fname]
end


-- Create the training batch
local traincache = Cache(opt.scpfile,opt.cachesize,true,opt.fext)

local mean,var = nil
if (opt.globalnorm ~= "")then
	mean,var = readglobalnorm(opt.globalnorm)
else
	print(sys.COLORS.red .. "==> Global norm not given, trying to estimate means and variances from data")
	mean,var = traincache:accstats()
end

local cvbatch = nil
local trainbatch = nil
if opt.model:starts("jvector") then
	-- 3 targets jvector (speaker,attacktype and gender)
	trainbatch = JVectorBatch(traincache,true,opt.batchSize,datadim,mean,var,3)
else
	trainbatch = Batchiterator(traincache,true,opt.batchSize,datadim,mean,var)
end
if (opt.cvscpfile~='')then
	-- Create a new cache, but do not use any randomization
	local cvcache = Cache(opt.cvscpfile,opt.cachesize,false,opt.fext)
	cvbatch = nil
	if opt.model:starts("jvector") then
		cvbatch = JVectorBatch(cvcache,false,opt.batchSize,datadim,mean,var,3)
	else
		cvbatch = Batchiterator(cvcache,false,opt.batchSize,datadim,mean,var)
	end
	cvtime,besterr = cvmain(cvbatch)
end

while (trainIter < opt.minIter) do
	-- Time for one iteration
	trainIter = trainIter+1
	-- Iterator to the traindata, we need to recall the function to reset the iterator

	-- local trainbatchiter = trainbatch:batchiter()
	-- The training size is not essentially correct, since it depends on the opt.filenum how many "leftovers " we obtain
	traintime,trainerr,trainacc = trainmain(trainbatch)
	if(opt.cvscpfile ~= '') then
		cvtime,cverr,cvacc = cvmain(cvbatch)
	end

	local bestfilename = paths.concat(opt.save, opt.ldmodel)
	local iterfilename = paths.concat(opt.save, opt.ldmodel.."_iter_" .. trainIter)
	local curracc = cvacc or trainacc
	os.execute('mkdir -p ' .. sys.dirname(bestfilename))

	-- print('==> current error is '.. currerr .. '\n')
	print('==> saving iteration model of iteration ' .. trainIter .. ' to '..iterfilename)
	if (curracc > bestacc) then
		bestacc = curracc
		opt.learningRate = opt.learningRate/2
		print('==> saving current best model of to '..bestfilename)
		torch.save(bestfilename, model)
	else
	    if fileexist(bestfilename) then
	        print('==> Restoring best model')
	        model = torch.load(bestfilename,'binary')
	    end
	end
	-- Save the current iteration
	torch.save(iterfilename, model)
	statslog:add{string.format("%i", trainIter),string.format("%.1fs", traintime),string.format("%.2f%%", confusionTrain.totalValid*100),string.format("%.2f%%", confusionTrain.averageValid*100),
	string.format("%.2f%%", confusionTrain.averageUnionValid*100),string.format("%.2f", trainerr),"Train"}
	if(opt.cvscpfile ~= '') then
		statslog:add{string.format("%i", trainIter),string.format("%.1fs", cvtime),string.format("%.2f%%", confusionCV.totalValid*100),string.format("%.2f%%", confusionCV.averageValid*100),
		string.format("%.2f%%", confusionCV.averageUnionValid*100),string.format("%.2f", cverr),"CrossVal"}
	end

	accLogger:add{['% train accuracy'] = confusionTrain.totalValid*100, ['% test accuracy'] = confusionCV.totalValid*100}
	errLogger:add{['train error']    = trainerr, ['test error']    = cverr}


	if (opt.plot) then
		-- plot logger
		accLogger:style{['% train accuracy'] = '-', ['% test accuracy'] = '-'}
		errLogger:style{['train error']    = '-', ['test error']    = '-'}
		accLogger:plot()
		errLogger:plot()
	end

	-- Just in case we allocated somewhat too much memory
	collectgarbage()
end


