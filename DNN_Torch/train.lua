-- :CUDA?
if opt.type == 'cuda' then
    model:cuda()
    criterion:cuda()
end
----------------------------------------------------------------------
-- classes
classes = {}
for i=1,opt.noutputs do
    classes[i] = ''..i
end

-- Offset for later scoring, to correctly specify the given labels
offsets={0}
-- If we have mutliple outputs we append these to the output classes
if (opt.nouttypes~=0) then
    for i=opt.noutputs,opt.noutputs+opt.nouttypes do
        classes[i] = ''..i
    end
    table.insert(offsets,opt.noutputs)
    table.insert(offsets,#classes)
    -- Also append the gender ones
    classes[#classes + 1] = ''..#classes + 1
    classes[#classes + 1] = ''..#classes + 1
end


-- This matrix records the current confusion for the current batch
confusionTrain = optim.ConfusionMatrix(classes)
confusionCV = optim.ConfusionMatrix(classes)
-- Retrieve parameters and gradients:
-- this extracts and flattens all the trainable parameters of the mode
-- into a 1-dim vector
if model then
    parameters,gradParameters = model:getParameters()
end


----------------------------------------------------------------------
print '==> configuring optimizer'

if opt.optimization == 'CG' then
    optimState = {
        maxIter = opt.maxIter
    }
    optimMethod = optim.cg

elseif opt.optimization == 'LBFGS' then
    optimState = {
        learningRate = opt.learningRate,
        maxIter = opt.maxIter,
        nCorrection = 10
    }
    optimMethod = optim.lbfgs

elseif opt.optimization == 'SGD' then
    optimState = {
        learningRate = opt.learningRate,
        weightDecay = opt.weightDecay,
        momentum = opt.momentum,
        learningRateDecay = 1e-6,
    }
    optimMethod = optim.sgd

elseif opt.optimization == 'ASGD' then
    optimState = {
        eta0 = opt.learningRate,
        t0 = trsize * opt.t0
    }
    optimMethod = optim.asgd
elseif opt.optimization == 'adagrad' then

    optimState = {

        learningRate = opt.learningRate,
        weightDecay = opt.weightDecay,
        momentum = opt.momentum,
        learningRateDecay = 1e-6,
    }
    optimMethod= optim.adagrad

elseif opt.optimization =='adadelta' then
    optimState = {
        rho= 0.9
    }
    optimMethod = optim.adadelta


else
    error('unknown optimization method')
end

----------------------------------------------------------------------
print '==> defining training procedure'

function trainbatch(inputs,targets)
    local toterr = 0

    -- create closure to evaluate f(X) and df/dX
    local feval = function(x)
        -- get new parameters
        if x ~= parameters then
            parameters:copy(x)
        end

        -- reset gradients
        gradParameters:zero()

        local preds = model:forward(inputs)
        local loss = 0

        loss = loss + criterion:forward(preds,targets)

        -- estimate df/dW
        -- normalize gradients and f(X) is done in criterion/backward
        local df_do = criterion:backward(preds, targets)

        model:backward(inputs, df_do)
        -- Checks if the output is a torch tensor (so we have a single output)
        if torch.typename(preds) then
            confusionTrain:batchAdd(preds,targets)
        else
            -- update confusion
            for i=1,#preds do
                -- Add the offset so that we "unroll" the given three class classifier
                confusionTrain:batchAdd(preds[i],targets[i]+offsets[i])
            end

        end

        -- Update grad Parameters per batch
        gradParameters:div(inputs:size(1))

        -- Per sample error rate
        toterr = toterr + loss
        -- return f and df/dX
        return loss,gradParameters
    end
    -- optimize on current mini-batch
    if optimMethod == optim.asgd then
        _,_,average = optimMethod(feval, parameters, optimState)
    else
        optimMethod(feval, parameters, optimState)
    end

    return toterr

end


function crossvalidatebatch(inputs,targets)

    -- averaged param use?
    if average then
        cachedparams = parameters:clone()
        parameters:copy(average)
    end

    local preds = model:forward(inputs)

    local loss = 0
    -- In case of jvector we have multiple outputs
    if opt.model:starts('jvector') then

        loss = loss + criterion:forward(preds,targets)
        -- loss = loss + uttcriterion:forward(preds[2],targets[2])

        for i=1,#preds do
            -- Add the offset so that we "unroll" the given three class classifier
            confusionCV:batchAdd(preds[i],targets[i]+offsets[i])
        end
    else
        loss = criterion:forward(preds,targets)
        confusionCV:batchAdd(preds,targets)
    end

    return loss
end



