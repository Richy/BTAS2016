--Copyright (C) 2015  Hani Altwaijry
--Released under MIT License
--license available in LICENSE file

require 'torch'
require 'libhtktoth'
require 'xlua'

local htktoth = {}

local help = {
loadhtk = [[Loads a htk file to a torch.Tensor]],
loadheader = [[Loads only the header of a given Filepath]]
}

htktoth.loadhtk = function(filepath)
                   if not filepath then
                      xlua.error('file path must be supplied',
                                  'htktoth.loadhtk',
                                  help.loadhtk)
                   end
                   return libreadhtk.loadhtk(filepath)
                end

htktoth.loadheader = function(filepath)
                    if not filepath then
                        xlua.error("Filepath must be supplied",'htktoth.loadheader',help.loadheader)
                    end
                    return libreadhtk.loadheader(filepath)
                end

return htktoth
