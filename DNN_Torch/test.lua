print '==> executing all'

-- Also includes data and model
require 'init'
----------------------------------------------------------------------
print("==> Testing given dataset")

if not opt.scpfile then
    error("Please specify a file containing the data with -scpfile")
    return
elseif io.open(opt.scpfile,"rb") == nil then
    error(string.format("Given scp file %s cannot be found!",opt.scpfile))
    return
end

function count(arr)
    local maxelem,ind = arr:max(1)
    elements = torch.zeros(maxelem[1][1])
    for i=1,arr:size(1) do
        local elem = arr[i][1]
        elements[elem]=elements[elem] + 1
    end

    return elements
end

-- extractfeats function
function testfeats(inputs)
   -- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
   model:evaluate()

   -- if opt.type == 'double' then inputs = inputs:double()
   if opt.type == 'cuda' then
       inputs = inputs:cuda()
   end

   local maxelem,maxrow = model:forward(inputs):max(2)
   local maxelem,ind = count(maxrow):max(1)
   return ind:int()
end

local outputfile = torch.DiskFile(opt.testoutput, 'w')
print('==> Outputfile can be found at ',outputfile)

-- For the usual case our inputs are one dimensional
local datadim = { ninputs }
if (opt.model == 'convnet') then
    datadim = { width , nfeats, height }
end


local testcache = FileCache(opt.scpfile,opt.cachesize,false,opt.fext)


local testiterator = Chunkiterator(testcache,false,opt.batchSize,datadim,mean,var)

-- We do not need any labels for extracting the features
local testbatchiter = testiterator:iterator(function(line) return 1 end)
local batchnum = 0
local starttime = torch.tic()
for inputs,targets,filename in testbatchiter do
    batchnum = batchnum + 1
    xlua.progress(batchnum,testcache._uttsize)
    outputfile:writeString(filename)
    outputfile:writeString(" ")
    outputfile:writeInt(testfeats(inputs)[1])
    collectgarbage()
end
