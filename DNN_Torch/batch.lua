-- This class provides a batch interface for any kinds of HTK based tasks for Torch
-- To use the classes, we have a simple interface, which consists of two components
-- One is

require 'libhtktoth'
local torch = require 'torch'
local class = torch.class

local yield = coroutine.yield
local wrap = coroutine.wrap

-- Only for debugging
-- torch.setdefaulttensortype('torch.FloatTensor')

local function readfilelines(filepath)
    local f = assert(io.open(filepath,'r'))
    local line = f:read()

    local lines ={}
    while(line~=nil) do
        lines[#lines+1] = line
        line=f:read()
    end
    f:close()
    return lines
end

local function shallowcopy(orig,copy)
    for orig_key, orig_value in pairs(orig) do
        copy[orig_key] = orig_value
    end
    return copy
end

local function permute(tab, n, count)
    n = n or #tab
    for i = 1, count or n do
        local j = math.random(i, n)
        tab[i], tab[j] = tab[j], tab[i]
    end
    return tab
end


do
    local Cache = class("Cache")

    function Cache:__init(filename,size,shuffle,frameExt)
        assert(filename ~= nil,"Given filename is nil ")
        -- Read in only the filenames and store as a table
        self._filecontent = readfilelines(filename)
        self._uttsize = #self._filecontent
        self._shuffle = shuffle == nil
        -- Some default value for the cache
        self.cachesize = size or 16384
        -- Frame extension
        self.frameExt = frameExt or 0

        -- Single dimensional feature. samplesize is stored as a float, so size is *4
        local singledim = loadheader(self._filecontent[1]).samplesize/4
        -- As a backup for Batch to infer the current datadim from the actual data
        self.datadim = 2*singledim * self.frameExt + singledim

        -- Inner coroutine to get the current size of the network
        self._getsize = coroutine.create(function()
            local samples = 0
            for i=1,self._uttsize do
                samples = samples + loadheader(self._filecontent[i]).nsamples
                yield(samples)
            end
            return samples
        end)
        -- Size is the final size which will be stored in the function self.size
        local _size = 0
        self.size = function ()
            if self._getsize ~= nil and
                coroutine.status (self._getsize)  == "suspended" then
                _,_size = coroutine.resume(self._getsize)
            end
            return _size
        end

    end

    -- Provide a function getfilelabel, which declaration is:
    -- function getfilelabel(line)
    --  return line:split("")
    -- end
    -- You just need to return the label for the given line
    function Cache:iterator(getfilelabel)
        return wrap(function() return self:_iterator(getfilelabel) end)
    end

    -- Returns the -mean and the 1./stdv from the data
    function Cache:accstats()
        globalstd = torch.zeros(self.datadim)
        globalmean = torch.zeros(self.datadim)
        -- Dummy function, since we dont need the labels at all
        local iter = self:iterator(function() return 1 end)
        for inputs,targets in iter do
            local inputtensor = torch.Tensor(#inputs,inputs[1]:size(1))
            for i=1,inputtensor:size(1) do
                inputtensor[i] = inputs[i]
            end
            globalmean:add(inputtensor:mean(1))
            globalstd:add(inputtensor:std(1))
        end
        globalstd = torch.ones(globalstd:size()):cdiv(globalstd)
        return -globalmean,globalstd
    end

    -- Help function to do the actual iteration
    function Cache:_iterator(getfilelabel)
        local featscache = {}
        local labelscache = {}
        local finecontents
        -- Shuffle the conents in advance
        if (self.shuffle) then
            finecontents = permute(self._filecontent)
        else
            finecontents = self._filecontent
        end
        local line = nil
        local iterind = 1
        local feat = nil
        repeat
            local feats ={}
            local labels = {}
            local chunk = nil
            local filename = nil
            local label = nil
            -- Restore features from the cache
            shallowcopy(featscache,feats)
            shallowcopy(labelscache,labels)
            local cursize = #feats
            repeat
                line=finecontents[iterind]
                if(line == nil) then
                    break
                end
                -- Listfile is an iterator to the filelist
                filename = paths.basename(line)
                assert(io.open(line),"File " .. line .. " does not exist!")
                -- Using the frame extension of the current model, we obtain the feature as torch tensor
                feat = loadhtk(line, self.frameExt)
                label = getfilelabel(filename)
                -- If the read feature is too large , we remove it and store it later in the featcache
                for j=1,math.min(feat:size(1),self.cachesize - #feats) do
                    feats[#feats+1] = feat[j]
                    labels[#feats] = label
                end
                cursize = cursize + feat:size(1)
                iterind = iterind + 1
            until (#feats >= self.cachesize)
            -- The difference between the actual feature size and our maximum capability
            local diff = cursize - self.cachesize
            local i = 1
            -- Reset the table values from the previous iteration
            for k in next, featscache do rawset(featscache, k, nil) end
            for k in next, labelscache do rawset(labelscache, k, nil) end
            -- Fill in the elements into the cache, if we have any
            for j=feat:size(1)-diff+1,feat:size(1) do
                featscache[i] = feat[j]
                labelscache[i] = label
                i = i + 1
            end
            -- Next iteration of indices
            yield(feats,labels,filename)
        until(iterind > self._uttsize)
    end

end

do
    -- This class only yields every read in file as a batch, thus is not consistent with its size
    -- Better only use it with a very large cache
    local FileCache = class ('FileCache','Cache')

    function FileCache:__init(filename,size,shuffle,frameExt)
        Cache.__init(self,filename,size,shuffle,frameExt)
    end

    function FileCache:_iterator(getfilelabel)
        local filecontents
        -- Shuffle the conents in advance
        if (self.shuffle) then
            filecontents = permute(self._filecontent)
        else
            filecontents = self._filecontent
        end
        local line = nil
        local iterind = 1
        local feat = nil
        repeat
            local feats ={}
            local labels = {}
            line=filecontents[iterind]
            if(line == nil) then
                break
            end
            -- Listfile is an iterator to the filelist
            local filename = paths.basename(line)
            assert(io.open(line),"File " .. line .. " does not exist!")
            -- Using the frame extension of the current model, we obtain the feature as torch tensor
            feat = loadhtk(line, self.frameExt)
            local label = getfilelabel(filename)
            -- If the read feature is too large , we remove it and store it later in the featcache
            for j=1,feat:size(1) do
                feats[#feats+1] = feat[j]
                labels[#feats] = label
            end
            iterind = iterind + 1
            -- Next iteration of indices
            yield(feats,labels,filename)
        until(iterind > self._uttsize)
    end
end



local Iterator = class("Iterator")
function Iterator:__init(cache,shuffle,batchsize,datadim,mean,var)
    assert(cache ,"You need to specify a cache by init a cache Object Cache()")
    self.shuffle = self.shuffle ==nil
    self.batchsize = batchsize or 128
    self.cache = cache
    -- Per default we assume having 1 dimensional data given by the data itself
    self.datadim = datadim or { self.cache.datadim }

    self.mean = mean
    self.var = var
    self.size = function() return self.cache:size() end

    self.storage = torch.LongStorage(#self.datadim+1)
    local dim = 1
    -- Set the storage from the datadim dimensions. First dimension can be the batch
    for i=1,#self.datadim do
        self.storage[i+1] = self.datadim[i]
        dim = dim * self.datadim[i]
    end
    self.dim = dim

    if self.mean and self.var then
        local dim = 1
        for i=1,#self.datadim do
            dim = dim * self.datadim[i]
        end
        assert(self.mean:size(1) == dim,string.format("Size of data ( %i ) does not match size of mean ( %i ) ",self.mean:size(1),dim))
        self._normalize = function(x)
            -- Normalize it, we assume that mean is negative = - mean and variance is already divded by 1./var
            return (x:add(self.mean)):cmul(self.var)
        end
    else
        -- No normalization at all
        self._normalize = function(x)
            return x
        end
    end
end

function Iterator:iterator(getfilename)
    return wrap(function() return self:_iterator(getfilename) end)
end

function Iterator:_iterator(getfilename)
    local iter = self.cache:iterator(getfilename)
    for inputs,targets in iter do
        batchit = self:_iterate(inputs,targets,getfilename)
        for batchinp,batchtar in batchit do
            yield(batchinp,batchtar)
        end
    end
end

function Iterator:_iterate(inputs,targets,filename)
    return wrap(function() return self:iterate(inputs,targets,filename) end)
end



local Batchiterator = class('Batchiterator','Iterator')

function Batchiterator:__init(cache,shuffle,batchsize,datadim,means,vars)
    Iterator.__init(self,cache,shuffle,batchsize,datadim,means,vars)
end

function Batchiterator:iterate(inputs,targets)
    assert(#inputs == #targets,string.format("Number of inputs ( %i ) not equal to number of targets ( %i )",#inputs,#targets))
    local shuffleddata
    local inputtensor

    local targettensor
    -- We always shuffle the input file from the cache
    if (self.shuffle) then
        shuffleddata = torch.randperm(#inputs)
    else
        shuffleddata = torch.range(1,#inputs)
    end
    -- Check if the given dims are right
    assert(self.dim == inputs[1]:squeeze():size(1),string.format("Given datadims ( %i ) do not match the input dimensions ( %i )",self.dim,inputs[1]:squeeze():size(1)))
    for t = 1,#inputs,self.batchsize do
        self.storage[1]=math.min(self.batchsize,#inputs-t+1)
        inputtensor = torch.Tensor(self.storage)
        targettensor = torch.Tensor(self.storage[1])

        for i = t,math.min(t+self.batchsize-1,#inputs) do
            -- load new sample and permute the input
            inputtensor[i-t+1] = self._normalize(inputs[shuffleddata[i]])
            targettensor[i-t+1] = targets[shuffleddata[i]]
        end

        yield(inputtensor, targettensor)
    end
end

-- Iterates over the whole chunk which is given, does not generate batches
local Chunkiterator = class('Chunkiterator','Iterator')

function Chunkiterator:__init(cache,shuffle,batchsize,datadim,means,vars)
    Iterator.__init(self,cache,shuffle,batchsize,datadim,means,vars)
end

function Chunkiterator:_iterator(getfilename)
    local iter = self.cache:iterator(getfilename)
    for inputs,targets,filename in iter do
        batchit = self:_iterate(inputs,targets,filename)
        for batchinp,batchtar,filename in batchit do
            yield(batchinp,batchtar,filename)
        end
    end
end

function Chunkiterator:iterate(inputs,targets,filename)
    assert(#inputs == #targets,string.format("Number of inputs ( %i ) not equal to number of targets ( %i )",#inputs,#targets))
    local shuffleddata
    local inputtensor
    local targettensor
    -- We always shuffle the input file from the cache
    if (self.shuffle) then
        shuffleddata = torch.randperm(#inputs)
    else
        shuffleddata = torch.range(1,#inputs)
    end
    local s = torch.LongStorage(#self.datadim+1)
    local dim = 1
    for i=1,#self.datadim do
        s[i+1] = self.datadim[i]
        dim = dim * self.datadim[i]
    end
    -- Check if the given dims are right
    assert(dim == inputs[1]:squeeze():size(1),string.format("Given datadims ( %i ) do not match the input dimensions ( %i )",self.dim,inputs[1]:squeeze():size(1)))
    s[1]=#inputs
    inputtensor = torch.Tensor(s)
    targettensor = torch.LongTensor(s[1])

    for i = 1,#inputs do
        -- load new sample and permute the input
        inputtensor[i] = self._normalize(inputs[shuffleddata[i]])
        targettensor[i] = targets[shuffleddata[i]]
    end
    yield(inputtensor, targettensor,filename)
end

local JVectorBatch = class('JVectorBatch','Iterator')


function JVectorBatch:__init(cache,shuffle,batchsize,datadim,means,vars,numout)
    Iterator.__init(self,cache,shuffle,batchsize,datadim,means,vars)
    -- The number of indipendent targets
    self.numout = numout
end


function JVectorBatch:iterate(inputs,targets)
    -- assert(#inputs == #targets,string.format("Number of inputs ( %i ) not equal to number of targets ( %i )",#inputs,#targets))
    local shuffleddata
    local inputtensor
    local targettensor
    -- We always shuffle the input file from the cache
    if (self.shuffle) then
        shuffleddata = torch.randperm(#inputs)
    else
        shuffleddata = torch.range(1,#inputs)
    end
    -- Check if the given dims are right
    assert(self.dim == inputs[1]:squeeze():size(1),string.format("Given datadims ( %i ) do not match the input dimensions ( %i )",self.dim,inputs[1]:squeeze():size(1)))
    for t = 1,#inputs,self.batchsize do
        self.storage[1]=math.min(self.batchsize,#inputs-t+1)
        inputtensor = torch.Tensor(self.storage)
        targettensor = torch.LongTensor(self.numout,self.storage[1])

        for i = t,math.min(t+self.batchsize-1,#inputs) do
            -- load new sample and permute the input
            inputtensor[i-t+1] = self._normalize(inputs[shuffleddata[i]])
            local shuffledtarget = targets[shuffleddata[i]]
            assert(#shuffledtarget == self.numout,"Error, did not find some of the labels of the data.")
            for j = 1,#targets[shuffleddata[i]] do
                for out=1,self.numout do
                    -- Targets for the speaker id output /
                    -- Targets for the utterance id output
                    targettensor[out][i-t+1] = shuffledtarget[out]

                end
            end
        end
        yield(inputtensor, targettensor)
    end
end




