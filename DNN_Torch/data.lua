math.randomseed(os.time())
----------------------------------------------------------------------

function fileexist(name)
    local f=io.open(name,"r")
    if f~=nil then io.close(f) return true else return false end
end

function readLabel(filename)
    filelabel = {}
    nlabels = 0
    if (filename~='') then
        local fin = io.open(filename,'r')
        assert(fin,"File "..filename.." does not exist!")
        for line in fin:lines() do
            local l = line:split(' ')
            -- Filelabels are UTTNAME TYPEID SPKID MALEFEMALE
            typeid=tonumber(l[2])
            spkid=tonumber(l[3])
            gender=tonumber(l[4])
            if typeid and spkid and gender then
                filelabel[l[1]] = {typeid,spkid,gender}
            else
                filelabel[l[1]] = typeid
            end
            nlabels = nlabels+1
        end
    end
    return filelabel,nlabels
end

function permute(tab, n, count)
    n = n or #tab
    for i = 1, count or n do
        local j = math.random(i, n)
        tab[i], tab[j] = tab[j], tab[i]
    end
    return tab
end

function readfilelines(filepath)
    local f = assert(io.open(filepath,'r'))
    local line = f:read()

    local lines ={}
    while(line~=nil) do
        lines[#lines+1] = line
        line=f:read()
    end
    f:close()
    return lines
end


function list_iter(t)
    local i = 0
    -- In case its nil we simply return an empty table
    t = t or {}
    local n = #t or table.getn(t)
    return function ()
        i = i + 1
        if (i <= n) then
            return t[i]
        end
    end
end



