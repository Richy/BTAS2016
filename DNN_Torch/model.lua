----------------------------------------------------------------------
-- reading parameters from a txt file, usually trained by TNET or other tools

-- load a model from a binary file trained by previous torch work or create a new model if could not find a model in save path
-- function modelfromfile()
--    print '==> load model'

--    local filename = paths.concat(opt.save, opt.ldmodel)
--    model = io.open(filename, 'rb')
function loadmodelfromfile(filename)
   -- Nstates is only used if we need to extend the current model / change it
      print (sys.COLORS.green..'Model found  '..filename)
      model:close()
      model = torch.load(filename)
      return model
      -- parameters

end


function extendModel(model,nstates)
      local ninputs = model:get(1).weight:size(2)
      -- If our model has already init some layers (typically after training once), we load the outputs
      -- of the layers into the current model, otherwise skip them
      if (model.modules[#model.modules].output:dim() ~= 0)then
         noutputs = model.modules[#model.modules].output:size(2)
      end

      -- linear_nodes = model:findModules('nn.Linear')
      -- -- First and last linear nodes are not hidden layers
      -- nhidden_layers = #linear_nodes - 2

      -- -- current hidden layer
      -- local curhid = (model:size()-#linear_nodes)/nhidden_layers+1  -- 5 is the number of input and output layers (Lin, Re, Lin, LogSM)
      -- If we changed the shape of the current model!
      if (opt.model=='deepneunet' and curhid<opt.hidlaynb and opt.hidlaynb~=0 and opt.hidlaynb<=#nstates) then
         table.remove(model.modules, #model.modules)
         -- table.remove(model.modules, #model.modules)
         for i=1, opt.hidlaynb-curhid do
            model:add(nn.Linear(nstates[curhid],nstates[curhid+1]))
            model:add(nn.PReLU(),model:size())
            model:add(nn.BatchNormalization(nstates[curhid+1]))
            curhid = curhid+1
         end
         model:add(nn.Linear(nstates[curhid],noutputs))
         model:add(nn.SoftMax())
      elseif (opt.hidlaynb>#nstates) then
         print("Warning: too many layers to build, not enough nstates")
      end
      return model
end

function poolingMethod(method,model,lwidth,lheight,poolsizew,poolsizeh,dpoolsizew,dpoolsizeh,nInputPlane,pnorm)
   local tmph,tmpw
   if (method=='MaxP') then	-- MaxPooling
      model:add(nn.SpatialMaxPooling(poolsizew,poolsizeh,dpoolsizew,dpoolsizeh))
      tmph = math.floor((lheight-poolsizeh)/dpoolsizeh+1)
      tmpw = math.floor((lwidth-poolsizew)/dpoolsizew+1)

   elseif (method=='LPP') then   -- LPPooling
      model:add(nn.SpatialLPPooling(nInputPlane, pnorm, poolsizew, poolsizeh, dpoolsizew, dpoolsizeh))
      tmph = math.floor((lheight-poolsizeh)/dpoolsizeh+1)
      tmpw = math.floor((lwidth-poolsizew)/dpoolsizew+1)

   elseif (method=='wavelet') then
      model:add(nn.SpatialWaveletPooling())
      tmph = math.ceil(lheight/2)
      tmpw = math.ceil(lwidth/2)
   end


   return tmpw, tmph, model
end

function newmodel(nstates,ninputs,noutputs,frameExt)
   -- print ('==> model in '..filename..' is not found')
   print (sys.COLORS.green..'==> construct model')

   if opt.model == 'jvectorELU' then
      local nhidla = #nstates
      assert(opt.nouttypes~=0,"If using jvector, specify number of utts with -nouttypes")
      model = nn.Sequential()
      model:add(nn.Linear(ninputs,nstates[1]))
      model:add(nn.BatchNormalization(nstates[1]))
      model:add(nn.ELU(0.6))
      for i = 1,nhidla-1 do
         model:add(nn.Linear(nstates[i], nstates[i+1]))
         model:add(nn.BatchNormalization(nstates[i+1]))
         model:add(nn.ELU(0.4))
      end

      local jvectorout = nn.ConcatTable()

      jvectorout:add(nn.Linear(nstates[nhidla], noutputs))

      jvectorout:add(nn.Linear(nstates[nhidla], opt.nouttypes))
      -- Gender
      jvectorout:add(nn.Linear(nstates[nhidla], 2))

      model:add(jvectorout)
      -- Append the Logsoftmax for each output
      local maxlayers = nn.ParallelTable()
      maxlayers:add(nn.LogSoftMax())
      maxlayers:add(nn.LogSoftMax())
      maxlayers:add(nn.LogSoftMax())

      model:add(maxlayers)
      -- Fuse the two parts again together, using the 2nd dimension
      -- model:add(nn.JoinTable(2))

   elseif opt.model == 'ELU' then
      local nhidla = #nstates
      model = nn.Sequential()
      model:add(nn.Linear(ninputs,nstates[1]))
      model:add(nn.BatchNormalization(nstates[1]))
      model:add(nn.ELU(0.6))
      for i = 1,nhidla-1 do
         model:add(nn.Linear(nstates[i], nstates[i+1]))
         model:add(nn.BatchNormalization(nstates[i+1]))
         model:add(nn.Dropout(i*0.1))
         model:add(nn.ELU(0.4))
      end

      model:add(nn.Linear(nstates[nhidla], opt.nouttypes))
      model:add(nn.LogSoftMax())
    elseif opt.model =='RReLU' then
      local nhidla = #nstates
      model = nn.Sequential()
      model:add(nn.Linear(ninputs,nstates[1]))
      model:add(nn.BatchNormalization(nstates[1]))
      model:add(nn.RReLU())
      for i = 1,nhidla-1 do
         model:add(nn.Linear(nstates[i], nstates[i+1]))
         model:add(nn.BatchNormalization(nstates[i+1]))
         model:add(nn.Dropout(i*0.1))
         model:add(nn.RReLU())
      end

      model:add(nn.Linear(nstates[nhidla], opt.nouttypes))
      model:add(nn.LogSoftMax())

   elseif opt.model == 'convnet' then


      -- a typical modern convolution network (conv+relu+pool)
      local curState=1
      local loch1,locw1,loch2,locw2
      model = nn.Sequential()

      -- stage 1 : filter bank -> squashing -> L2 pooling -> normalization
      model:add(nn.SpatialConvolutionMM(nfeats, nstates[curState], opt.filtsizew, opt.filtsizeh, opt.dfiltsizew, opt.dfiltsizeh))
      model:add(nn.SpatialBatchNormalization(nstates[curState]))
      model:add(nn.PReLU())
      loch1 = math.floor((height-filtsizeh)/dfiltsizeh+1)
      locw1 = math.floor((width-filtsizew)/dfiltsizew+1)
      --model:add(nn.SpatialMaxPooling(opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh))
      locw2,loch2,model = poolingMethod(opt.poolMethod,model,locw1,loch1,opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh,nstates[curState],opt.pnorm)
      -- Given width of filter is larger than the actual width of the features
      assert(locw2>0, string.format("filtsizew is too large, given %i, but the actual size is %i",opt.filtsizew,locw2))
      assert(loch2>0, string.format("filtsizeh is too large, given %i, but the actual size is %i",opt.filtsizeh,loch2))
      curState = curState+1

      -- stage 2 : filter bank -> squashing -> L2 pooling -> normalization
      model:add(nn.SpatialConvolutionMM(nstates[curState-1], nstates[curState], opt.filtsizew, opt.filtsizeh, opt.dfiltsizew, opt.dfiltsizeh))    -- output size is {nstates[curState], height2-filtsizeh+1, width2-filtsizew+1}
      model:add(nn.SpatialBatchNormalization(nstates[curState]))
      model:add(nn.PReLU())
      loch1 = math.floor((loch2-filtsizeh)/dfiltsizeh+1)
      locw1 = math.floor((locw2-filtsizew)/dfiltsizew+1)
      -- model:add(nn.SpatialMaxPooling(opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh))                -- output size is {nstates[curState], (height2-filtersizeh+1)/poolsize, (width2-filtersizew+1)/poolsize}
      locw2,loch2,model = poolingMethod(opt.poolMethod,model,locw1,loch1,opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh,nstates[curState],opt.pnorm)                -- output size is {nstates[curState], (height2-filtersizeh+1)/poolsize, (width2-filtersizew+1)/poolsize}
      -- Given width of filter is larger than the actual width of the features
      assert(locw2>0, string.format("filtsizew is too large, given %i, but the actual size is %i",opt.filtsizew,locw2))
      assert(loch2>0, string.format("filtsizeh is too large, given %i, but the actual size is %i",opt.filtsizeh,loch2))
      curState = curState+1

      -- stage 3 : standard 2-layer neural network
      model:add(nn.Reshape(nstates[curState-1]*locw2*loch2))
      model:add(nn.Linear(nstates[curState-1]*locw2*loch2,nstates[curState]))
      model:add(nn.BatchNormalization(nstates[curState]))
      model:add(nn.PReLU())
      model:add(nn.BatchNormalization(nstates[curState-1]))
      for i = curState,#nstates-1 do
         model:add(nn.Linear(nstates[i], nstates[i+1]))
         model:add(nn.BatchNormalization(nstates[i+1]))
         model:add(nn.PReLU())
      end
      model:add(nn.Linear(nstates[#nstates], noutputs))
      model:add(nn.LogSoftMax())

   elseif opt.model == 'convnet_ELU' then
      -- a typical modern convolution network (conv+relu+pool)
      local curState=1
      local loch1,locw1,loch2,locw2
      model = nn.Sequential()

      -- stage 1 : filter bank -> squashing -> L2 pooling -> normalization
      model:add(nn.SpatialConvolutionMM(nfeats, nstates[curState], opt.filtsizew, opt.filtsizeh, opt.dfiltsizew, opt.dfiltsizeh))
      model:add(nn.SpatialBatchNormalization(nstates[curState]))
      model:add(nn.ELU(0.6))
      model:add(nn.SpatialDropout())
      loch1 = math.floor((height-filtsizeh)/dfiltsizeh+1)
      locw1 = math.floor((width-filtsizew)/dfiltsizew+1)
      --model:add(nn.SpatialMaxPooling(opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh))
      locw2,loch2,model = poolingMethod(opt.poolMethod,model,locw1,loch1,opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh,nstates[curState],opt.pnorm)
      -- Given width of filter is larger than the actual width of the features
      assert(locw2>0, string.format("filtsizew is too large, given %i, but the actual size is %i",opt.filtsizew,locw2))
      assert(loch2>0, string.format("filtsizeh is too large, given %i, but the actual size is %i",opt.filtsizeh,loch2))
      curState = curState+1

      -- stage 2 : filter bank -> squashing -> L2 pooling -> normalization
      model:add(nn.SpatialConvolutionMM(nstates[curState-1], nstates[curState], opt.filtsizew, opt.filtsizeh, opt.dfiltsizew, opt.dfiltsizeh))    -- output size is {nstates[curState], height2-filtsizeh+1, width2-filtsizew+1}
      model:add(nn.SpatialBatchNormalization(nstates[curState]))
      model:add(nn.ELU(0.5))
      model:add(nn.SpatialDropout())
      loch1 = math.floor((loch2-filtsizeh)/dfiltsizeh+1)
      locw1 = math.floor((locw2-filtsizew)/dfiltsizew+1)
      -- model:add(nn.SpatialMaxPooling(opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh))                -- output size is {nstates[curState], (height2-filtersizeh+1)/poolsize, (width2-filtersizew+1)/poolsize}
      locw2,loch2,model = poolingMethod(opt.poolMethod,model,locw1,loch1,opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh,nstates[curState],opt.pnorm)                -- output size is {nstates[curState], (height2-filtersizeh+1)/poolsize, (width2-filtersizew+1)/poolsize}
      -- Given width of filter is larger than the actual width of the features
      assert(locw2>0, string.format("filtsizew is too large, given %i, but the actual size is %i",opt.filtsizew,locw2))
      assert(loch2>0, string.format("filtsizeh is too large, given %i, but the actual size is %i",opt.filtsizeh,loch2))
      curState = curState+1

      -- stage 3 : standard 2-layer neural network
      model:add(nn.Reshape(nstates[curState-1]*locw2*loch2))
      model:add(nn.Linear(nstates[curState-1]*locw2*loch2,nstates[curState]))
      model:add(nn.BatchNormalization(nstates[curState]))
      model:add(nn.ELU(0.4))
      --model:add(nn.Dropout(0.6))
      model:add(nn.BatchNormalization(nstates[curState-1]))
      for i = curState,#nstates-1 do
         model:add(nn.Linear(nstates[i], nstates[i+1]))
         model:add(nn.BatchNormalization(nstates[i+1]))
         model:add(nn.ELU(0.4))
      end
      model:add(nn.Linear(nstates[#nstates], noutputs))
      model:add(nn.LogSoftMax())

   elseif opt.model== 'convnet_ELU_deep' then

      -- a typical modern convolution network (conv+relu+pool)
      model = nn.Sequential()
      local curState=1
      local loch1,locw1
      local prevh = height
      local prevw = width

      --Append the nfeats as the zeroth element for the loop to work
      nstates[0]=nfeats
      for i=curState,#nstates-1 do
         model:add(nn.SpatialConvolutionMM(nstates[i-1], nstates[i], opt.filtsizew, opt.filtsizeh, opt.dfiltsizew, opt.dfiltsizeh))
         model:add(nn.SpatialBatchNormalization(nstates[i]))
         model:add(nn.ELU(0.5))
         model:add(nn.SpatialDropout())
         -- stage 1 : filter bank -> squashing -> L2 pooling -> normalization
         loch1 = math.floor((prevh-filtsizeh)/dfiltsizeh+1)
         locw1 = math.floor((prevw-filtsizew)/dfiltsizew+1)
         --model:add(nn.SpatialMaxPooling(opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh))
         prevw,prevh,model = poolingMethod(opt.poolMethod,model,locw1,loch1,opt.poolsizew,opt.poolsizeh,opt.dpoolsizew,opt.dpoolsizeh,nstates[curState],opt.pnorm)
         -- Given width of filter is larger than the actual width of the features
         assert(prevw>0, string.format("filtsizew is too large, given %i, but the actual size is %i (Layer %i)",opt.filtsizew,prevw,i))
         assert(prevh>0, string.format("filtsizeh is too large, given %i, but the actual size is %i (Layer %i)",opt.filtsizeh,prevh,i))
         -- Preserve the looping variable to the outer scope
         curState = i
      end

      model:add(nn.Reshape(nstates[curState-1]*prevw*prevh))
      model:add(nn.Linear(nstates[curState-1]*prevw*prevh,nstates[curState]))
      model:add(nn.BatchNormalization(nstates[curState]))
      model:add(nn.ELU(0.7))
      model:add(nn.Linear(nstates[#nstates], noutputs))
      model:add(nn.LogSoftMax())



   elseif opt.model == 'jvectorPReLU' then
      local nhidla = #nstates
      assert(opt.nouttypes~=0,"If using jvector, specify number of utts with -nouttypes")
      model = nn.Sequential()
      model:add(nn.Linear(ninputs,nstates[1]))
      model:add(nn.BatchNormalization(nstates[1]))
      model:add(nn.PReLU())
      for i = 1,nhidla-1 do
         model:add(nn.Linear(nstates[i], nstates[i+1]))
         model:add(nn.BatchNormalization(nstates[i+1]))
         model:add(nn.PReLU())
      end

      local jvectorout = nn.ConcatTable()

      -- speaker number outputs
      jvectorout:add(nn.Linear(nstates[nhidla], noutputs))
      -- Attack types ( Usually 9 )
      jvectorout:add(nn.Linear(nstates[nhidla], opt.nouttypes))
      -- The genders
      jvectorout:add(nn.Linear(nstates[nhidla], 2))

      model:add(jvectorout)
      -- Append the Logsoftmax for each output
      local maxlayers = nn.ParallelTable()
      maxlayers:add(nn.LogSoftMax())
      maxlayers:add(nn.LogSoftMax())
      maxlayers:add(nn.LogSoftMax())

      model:add(maxlayers)
      -- Fuse the two parts again together, using the 2nd dimension
      -- model:add(nn.JoinTable(2))

   else
      error('unknown -model')
   end


   -- Add the parameters which are given to the model, to save them for alter or when loading the model
   model.frameExt = frameExt
   model.nstates = nstates
   model.ninputs = ninputs

   model.width = width
   model.nfeats = opt.nfeats
   model.height = 2*opt.fext+1

   model.fext = opt.fext
   model.noutputs = opt.noutputs

   model.filtsizeh = opt.filtsizeh
   model.filtsizew = opt.filtsizew

   model.modeltype = opt.model

end

function printmodel()
   ----------------------------------------------------------------------
   print (sys.COLORS.green..'==> here is the model:')
   print(model)

   ----------------------------------------------------------------------
   -- Visualization is quite easy, using itorch.image().

   if opt.visualize then
      if opt.model == 'convnet' then
         if itorch then
        print '==> visualizing ConvNet filters'
        print('Layer 1 filters:')
        itorch.image(model:get(1).weight)
        print('Layer 2 filters:')
        itorch.image(model:get(5).weight)
         else
        print '==> To visualize filters, start the script in itorch notebook'
         end
      end
   end
end
