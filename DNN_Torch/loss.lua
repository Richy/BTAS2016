----------------------------------------------------------------------
print '==> define loss'
if opt.loss == 'margin' then

    -- This loss takes a vector of classes, and the index of
    -- the grountruth class as arguments. It is an SVM-like loss
    -- with a default margin of 1.

    --criterion = nn.MultiMarginCriterion()
    criterion = nn.MarginCriterion()

elseif opt.loss == 'nll' then

    -- This loss requires the outputs of the trainable model to
    -- be properly normalized log-probabilities, which can be
    -- achieved using a softmax function

    -- model:add(nn.LogSoftMax())

    -- The loss works like the MultiMarginCriterion: it takes
    -- a vector of classes, and the index of the grountruth class
    -- as arguments.

    criterion = nn.ClassNLLCriterion()

    -- Replace the criterion with a parallelcriterion
    if opt.model:starts('jvector') then
        -- false specifies that the targets are not repeated for different inputs ( we want different sized outputs)
        criterion = nn.ParallelCriterion(false):add(nn.ClassNLLCriterion()):add(nn.ClassNLLCriterion())
    end

    -- criterion = nn.CrossEntropyCriterion()

-- elseif opt.loss == 'mse' then

--     -- for MSE, we add a tanh, to restrict the model's output
--     model:add(nn.Tanh())

--     -- The mean-square error is not recommended for classification
--     -- tasks, as it typically tries to do too much, by exactly modeling
--     -- the 1-of-N distribution. For the sake of showing more examples,
--     -- we still provide it here:

--     criterion = nn.MSECriterion()
--     criterion.sizeAverage = false

--     -- Compared to the other losses, the MSE criterion needs a distribution
--     -- as a target, instead of an index. Indeed, it is a regression loss!
--     -- So we need to transform the entire label vectors:

else
    error('unknown -loss')
end

----------------------------------------------------------------------
print (sys.COLORS.green..'==> here is the loss function:')
print(criterion)

