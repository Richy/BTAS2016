#!/usr/bin/env python
import argparse
import subprocess as sub
import os
import errno
import re


ROOT = os.path.dirname(__file__)
CURDIR = os.getcwd()
# Currently tmpdir and logdir get overwritten in main()
TMP_DIR = os.path.join(CURDIR, 'tmp')
LOG_DIR = os.path.join(CURDIR, 'log')

# HTK Filepaths
HTKTOOLSDIR = '/slfs3/users/yl710/htk/HTKTools/'

HCOMPV = os.path.join(HTKTOOLSDIR, 'HCompV')
HCOPY = os.path.join(HTKTOOLSDIR, 'HCopy')
HLIST = os.path.join(HTKTOOLSDIR, 'HList')
HEREST = os.path.join(HTKTOOLSDIR, 'HERest')
HHED = os.path.join(HTKTOOLSDIR, 'HHEd')
HPARSE = os.path.join(HTKTOOLSDIR, 'HParse')
HRESULTS = os.path.join(HTKTOOLSDIR, 'HResults')
HVITE = os.path.join(HTKTOOLSDIR, 'HVite')

# HVTest which is basically a script using HVITE
HVTEST = '/home/slhome/hedi7/asr/tools/hvtest'


QSUB = 'qsub'


# ARguments for the HTECONFIG
HRPRUNE = [1000, 200, 4000]
HRGSCALE = 1


CONFIG_PREFIX_MAP = {

    'FORCEOUT': 'HREC',
    'FACTORLM': 'HNET',
    'TRACE': 'HNET',
    'TARGETKIND': 'HPARM',
    'NONUMESCAPES': 'HSHELL'

}


def executecommand(cmd, logfile=None):
    if logfile:
        with open(logfile, 'r') as logpointer:
            return sub.check_output(cmd, stdout=logpointer, stderr=logpointer)
    return sub.check_output(cmd)


def splitIntoChunks(l, num):
    '''
    Splits the given list (l) into num chunks.
    Chunks are roughly equal, yet if the number of elements is uneven,
    the last element will get the remaining elements
    '''
    a = []
    spl, ext = divmod(len(l), num)
    if ext:
	spl += 1
    for i in range(num):
        a.append(l[i * spl:(i + 1) * spl])
    # If he have a residual, we append the last entries into the last list
    return a


def runHVTest(corpus, mmfdir, HTEFile, testtype='test', logfile=None):
    cmd = [QSUB]
    cmd.append('-cwd')
    if logfile:
        cmd.extend('-o {}'.format(logfile).split())
    cmd.extend('-j y'.split())
    #cmd.extend('-t 1-{}'.format(njobs).split())
    # Parameter to guarantee that the job runs on the cluster
    # gpu.p is also possible
    cmd.append(HVTEST)

    cmd.append(HTEFile)
    cmd.append(testtype)
    cmd.append(corpus)
    cmd.append(mmfdir)
    executecommand(cmd)


def HList(filep, header=None, show_n=None):
    '''
    filep: path to a feature file
    show_n : Shows only the top n features ( integer )
    header : if true header is shown, otherwise not
    '''
    cmd = []
    cmd.append(HLIST)
    if header:
        cmd.append('-h')
    if show_n:
        cmd.extend('-e {}'.format(show_n).split())
    cmd.append(filep)
    return executecommand(cmd)


def getFeatureType(scpfile):
    '''
    This function reads in the first line of the given scp file and extracts from there on the given feature type
    '''
    # Only check the first line of the file, we want to use that as an example
    firstline = open(scpfile, 'r').readline().rstrip('\n')
    vals = firstline.split('=')
    examplepath = None
    # We have two cases either the given file has only features or is split between key value pairs
    # In any case we just take the path
    if len(vals) == 1:
        examplepath = vals[0]
    else:
        examplepath = vals[1]
    htkheader = HList(examplepath, header=True, show_n=1)
    return parse_HTK_header(htkheader)['Sample Kind']


def parse_HTK_header(hlistoutput):
    '''
    hlistoutput is a string, which was recieved calling HList(ARG,True)
    returns a dict which keys are the HTK arguments Sample Bytes,Sample Kind,Sample Period,File Format,Num Comps,Num Samples
    '''
    KEYWORDS = ['Sample Bytes', 'Sample Kind', 'Sample Period',
                'File Format', 'Num Comps', 'Num Samples']
    splitlines = hlistoutput.split('\n')
    if not re.findall('.*Source.*', hlistoutput):
        raise ValueError('given hlist output does not have a header! ')
    lines = iter(splitlines)
    # Config won't append any file, so it will only return one config of
    # anarbitrary file
    config = {}
    for line in lines:
        if re.search('.*Source.*', line):
            nextline = next(lines)
            while(not re.search('-.*Samples.*', nextline)):
                for keyword in KEYWORDS:
                    searched = re.search(keyword + ":\s+([\d\w_.]*)", nextline)
                    if searched:
                        config[keyword] = searched.group(1)
                nextline = next(lines)
    return config


def generate_hv_config(featuretype):
    HV_CONFIG = {
        'FORCEOUT': 'T',
        'FACTORLM': 'TRUE',
        'TRACE': '1',
        'TARGETKIND': featuretype,
        'NONUMESCAPES': 'T'
    }
    return HV_CONFIG


def writeHTEConfigFile(hteconfig):
    '''
    Writes a config file with the given hteconfig in the TMP dir and returns that path
    '''
    def setparam(param, value):
        return "{}:{}={}".format(CONFIG_PREFIX_MAP[param], param, value)
    htepath = os.path.join(TMP_DIR, 'config.cfg')
    with open(htepath, 'w') as htepointer:
        for param, value in hteconfig.items():
            htepointer.write(setparam(param, value))
            htepointer.write(os.linesep)

    return os.path.abspath(htepath)


def writeHTEFile(hteconfig, htepath):
    # MLFs and SCPs
    def setparam(param, value):
        return "{} {} = {}".format("set", param, value)

    with open(htepath, 'w') as htepointer:
        for param, value in hteconfig.items():
            htepointer.write(setparam(param, value))
            htepointer.write(os.linesep)


def generate_HVTest_config(corpuslists, mlffile, hvconfig, hmmlist, hvdict):
    '''
    Generates the HTE File for HVTest
    '''
    hrprunestr = map(float, HRPRUNE)
    hteconfigs = []
    # USually the bash scripts do reference TSCRIPT to a variable within another script
    # We dont want to do that, since it is very deceiving, so we just generate N configs
    # With the accordning TSCRIPT
    for corpus in corpuslists:
        HTE_CONFIG = {
            'SETLIST': "( {} )".format(" ".join(corpuslists)),
            'HRHMMLIST': hmmlist,
            'HVCONFIG': hvconfig,
            'ALIGNMLF': mlffile,
            'TYPELIST': '( {} )'.format('test'),
            'HRGSCALE': '( %.2f )' % (HRGSCALE),
            'HRPRUNE': '( \'{}\' )'.format(" ".join(map(str, hrprunestr))),
            'HVVOCAB': hvdict,
            'HVTRACE': '1',
            'HVNAME': HVITE,
            'TSCRIPT': corpus

        }
        hteconfigs.append(HTE_CONFIG)
    return hteconfigs


def splitCorpus(corpus, num):
    '''
    Splits the given corpus into the given number of pieces. Technically this means that the given corpus file is split up
    and the splits are written out into the same directory as the corpus file

    corpus: Filepath to the corpus, e.g. test.scp
    num: Number of pieces the corpus will be split into

    returns a list with the new splitted filepaths
    '''
    corpuslines = open(corpus).read().splitlines()
    pieces = splitIntoChunks(corpuslines, num)
    # Put the new pieces into the folder of the given corpus
    corpusdir = os.path.dirname(corpus)
    corpusname, ext = os.path.splitext(os.path.basename(corpus))
    piecepaths = []
    for i in range(len(pieces)):
        # The new piecename is corpusname_1 .... corpusname_N
        piecename = "{}_{}{}".format(corpusname, i, ext)
        piecepath = os.path.join(corpusdir, piecename)
        with open(piecepath, 'w') as piecepointer:
            piecepointer.write(os.linesep.join(pieces[i]))
        piecepaths.append(piecepath)

    return piecepaths


def getHMMList(mmfdir):
    '''
    Reads in the MMF file in mmfdir. Writes out a temporary HMMFile,
    which contains all the HMMnames within it and returns that filepath
    '''
    # There needs to be the MMF file within the given MMFDir
    hmms = []
    try:
        mmffile = os.path.join(mmfdir, 'MMF')
        with open(mmffile, 'r') as mmfpointer:
            for line in mmfpointer:
                found = re.search(ur'^~h "(.+)"', line)
                if found:
                    hmms.append(found.group(1))
        hmmlistpath = os.path.join(TMP_DIR, 'hmmlist')
        with open(hmmlistpath, 'w') as hmmpointer:
            for hmm in hmms:
                hmmpointer.write(hmm)
                hmmpointer.write(os.linesep)
        return hmmlistpath
    except OSError:
        raise(
            'MMF File needs to be in the given MMFdir directory (%s). ' % (mmfdir))


def generate_HV_Dict(hmmlist):
    '''
    Just writes out the keys from the given hmmlist two times in a row, e.g.
    hmmlist:
        UBM_male
    -> dict:
        UBM_male UBM_male
    returns the path to the generated dict file
    '''
    hmms = open(hmmlist, 'r').read().splitlines()
    dictoutput = os.path.join(TMP_DIR, 'dict')
    with open(dictoutput, 'w') as dictpointer:
        for hmm in hmms:
            dictpointer.write('{} {}'.format(hmm, hmm))
            dictpointer.write(os.linesep)
    return dictoutput


def makedirs():
    dirstomake = {v for (k, v) in globals().items() if k.endswith('_DIR')}
    map(mkdir_p, dirstomake)


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def parse_args():
    parser = argparse.ArgumentParser(
        'Does scoring in parallel. The file given in corpus will be scored against the model in the directory (dir).')
    parser.add_argument(
        'corpus', type=str, help='The file which is includes all the tests, eg: test.scp. It will be split and run on different jobs on the cluster')
    parser.add_argument(
        'mmfdir', type=str, help='Directory where the output will be put in AND the directory where the given MMF for the model is')
    parser.add_argument(
        'mlffile', type=str, help='MLFfile which indicates all the tests which should be done'
    )
    parser.add_argument(
         '-feat',dest='featuretype',type=str,help="The feature type"
    )
    parser.add_argument(
        '-nj', type=int, help='Number of jobs, default: %(default)s', default=4)
    parser.add_argument(
        '--hvtestpath', help='Path to the HVtest script which performs the HVite, default= %(default)s', default=HVTEST)
    return parser.parse_args()


def main():

    args = parse_args()
    #Just in case we want to run two different datasets in the same dir
    global TMP_DIR,LOG_DIR
    TMP_DIR = os.path.join(CURDIR,"tmp_"+os.path.dirname(args.mmfdir))
    LOG_DIR= os.path.join(CURDIR,"log_"+os.path.dirname(args.mmfdir))
    makedirs()

    # Split the Corpus into multiple smaller items
    piecepaths = splitCorpus(args.corpus, args.nj)
    if args.featuretype:
        featuretype = args.featuretype
    else:
        featuretype = getFeatureType(args.corpus)
    # Generate the the tests
    hmmlistpath = getHMMList(args.mmfdir)

    hvconfig = generate_hv_config(featuretype)

    hvdict = generate_HV_Dict(hmmlistpath)
    hvconfigpath = writeHTEConfigFile(hvconfig)

    # Generate the HTEFIle
    hteconfigs = generate_HVTest_config(
        piecepaths, args.mlffile, hvconfigpath, hmmlistpath, hvdict)
    hteconfigpaths = []
    for i in range(len(hteconfigs)):
        htepath = os.path.join(TMP_DIR, 'HTE_%i' % (i))
        hteconfigpaths.append(htepath)
        writeHTEFile(hteconfigs[i], htepath)

    for piecepath, hteconfigpath in zip(piecepaths, hteconfigpaths):
        piecename, _ = os.path.splitext(os.path.basename(piecepath))
        logpath = os.path.join(LOG_DIR, "%s.log" % (piecename))
        runHVTest(
            piecepath, args.mmfdir, hteconfigpath, logfile=logpath)

    # Preprocess the given .scp file to split it for the upcoming jobs
    # HTEFile is given
    # if args.HTEFile:

    # else:

if __name__ == '__main__':
    main()
