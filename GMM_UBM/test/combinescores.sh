#!/bin/bash
PROGNAME=$(basename $0)
function usage {

    # Display usage message on standard error
    echo "Usage: $PROGNAME scores outputscores" 1>&2
    echo "Error, please input the output directory of the align files and the output filename" 1>&2

}

function error_exit {

# Display error message and exit
echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
exit 1

}
if [[ $# != 2 ]]; then
    usage
    error_exit "Number of arguments given needs to be two!"
fi


scoresdir=$1
outputfile=$2


function combine {
    cat $scoresdir/align*/test_*.mlf | grep -v "MLF" | grep -v "^[\.]" | awk '{if($0 ~ /^\"/){printf("%s ",$0)}else{print $4/$2*100000}}' | sort |  sed 's/\"//g' > $outputfile
    echo "Done, scores can be found at: "$outputfile
}

combine




