#!/bin/bash
#Generates a score file given a real and a attack score file from the respective GMM's
PROGNAME=${0##*/}

function usage {

    echo "Usage: $PROGNAME SPOOFSCORES REALSCORES MLFFILE OUTPUTFILE" 1>&2
    echo "Error, please input the output directory of the align files and the output filename" 1>&2

}

# Pastes the input files a,b together and calculates the differences between the scores in a and b to c
function pasteinput {
    #$1 are the spoof scores
    #$2 are the real scores
    buf1=$(mktemp buf.XXXXXX)
    buf2=$(mktemp buf.XXXXXX)
    cat $1 | sort > $buf1
    cat $2 | sort > $buf2
    paste $buf1 $buf2 | awk '{e=split($1,a,"/"); print a[e],$4-$2}'>$3
}

function splitfilesup {
    #$1 is the input mlf file, $2 is the output file produced which is structured as : UTTERANCENAME classid
    cat $1 | awk '{if($0 ~ /^\"/){split($0,a,"/"); split(a[2],b,"."); getline; c[b[1]]=$1 }}END{for(i in c){ print i,c[i] }}' > $2
}

function combinetoone {
    #$1 is the combined real/impostor score file 
    #$2 is the output of the splitfulesup function. Its a file which has UTTERANCENAME CLASSIFID as format
    #$3 is the produced output file

    >$3
    #Read in the newly generated file to combine all the input files and the splitted file into a single file: UTTERANCE SCORE CLASSIFICATION
    while read -ra splitlines; 
        do 
            
            val=$(grep ${splitlines[0]} $1 | awk '{print $2}');
            #Break if the signal is provided
            test $? -gt 128 && break;
            echo ${splitlines[0]} $val ${splitlines[1]} >> $3;
        done < $2

}


#Main routine
if [[ $# -lt 4 ]]; then
    usage
    exit
fi

spoofinput=$1
realinput=$2
mlffile=$3
outputfile=$4

pasttempf=$(mktemp tmpscores.XXXXXX)
splittempf=$(mktemp tmpsplit.XXXXXX)

function cleanup {
    rm -rf $pasttempf
    rm -rf $splittempf
    rm -rf $buf1
    rm -rf $buf2
}
trap cleanup EXIT SIGKILL SIGINT SIGTERM
#Do the pasting of the files
echo "Pasting the input scores together..."
pasteinput $spoofinput $realinput $pasttempf
echo "Parsing MLF file..."
splitfilesup $mlffile $splittempf
echo "Writing the output file to: "$outputfile
combinetoone $pasttempf $splittempf $outputfile
echo "Done!"




