# The SJTU systems for BTAS 2016

This repository consists of the SJTU systems for the BTAS chellenge.

## Systems

The three used systems can be seen in the sub-folders. Note that we did not provide the post-processing scripts ( particularly LDA), which can be found [here](http://scikit-learn.org/0.16/modules/generated/sklearn.lda.LDA.html)

* GMM-UBM
* DNN-ELU using Torch
* LSTM using Keras

## Classifiers

For our final result we used the following classifiers:

1. 7 Layer DNN which is fed with a 30 frame context window input, using ELU activation with an alpha of 0.6 for the first and 0.4 for every other hidden layer. After each linear transformation we apply batch normalization and finally use a dropout which increases in its rate from 0.1 to 0.7 in 0.1 steps.
```lua
(1): nn.Linear(1209 -> 1024)
(2): nn.BatchNormalization
(3): nn.ELU (alpha:0.600000)
(4): nn.Linear(1024 -> 1024)
(5): nn.BatchNormalization
(6): nn.Dropout(0.100000)
(7): nn.ELU (alpha:0.400000)
(8): nn.Linear(1024 -> 1024)
(9): nn.BatchNormalization
(10): nn.Dropout(0.200000)
(11): nn.ELU (alpha:0.400000)
(12): nn.Linear(1024 -> 1024)
(13): nn.BatchNormalization
(14): nn.Dropout(0.300000)
(15): nn.ELU (alpha:0.400000)
(16): nn.Linear(1024 -> 1024)
(17): nn.BatchNormalization
(18): nn.Dropout(0.400000)
(19): nn.ELU (alpha:0.400000)
(20): nn.Linear(1024 -> 1024)
(21): nn.BatchNormalization
(22): nn.Dropout(0.500000)
(23): nn.ELU (alpha:0.400000)
(24): nn.Linear(1024 -> 1024)
(25): nn.BatchNormalization
(26): nn.Dropout(0.600000)
(27): nn.ELU (alpha:0.400000)
(28): nn.Linear(1024 -> 1024)
(29): nn.BatchNormalization
(30): nn.Dropout(0.700000)
(31): nn.ELU (alpha:0.400000)
(32): nn.Linear(1024 -> 4)
(33): nn.LogSoftMax
```


2. Three different BLSTM classifiers are used. We combine their outputs of dimension 512 (=HIDDEN_SIZE) to aggregate our gain.

    1. This model uses a plain LSTM implementation
            ```
            model.add_input(name='input', input_shape=(None,FEATURE_DIM))
            model.add_node(Masking(0.0),name='mask',input='input')
            model.add_node(TimeDistributedDense(HIDDEN_SIZE),name='transform',input='mask')
            model.add_node(LSTM(HIDDEN_SIZE/4,return_sequences=True),name='forward',input='transform')
            model.add_node(LSTM(HIDDEN_SIZE/4),name='backward',input='forward')
            model.add_node(Dense(HIDDEN_SIZE, activation='sigmoid'),name='representation',input='backward')
            model.add_node(Dropout(0.5),name='dropout',input='representation')
            model.add_node(Dense(OUTPUT_SIZE, activation='softmax'),name='softmax',input='dropout')
            model.add_output(name='output',input='softmax')
            ```
    2. This model is using BLSTM and a Gaussian Mixture Layer
            ```
            model.add_input(name='input', input_shape=(None,FEATURE_DIM))
            model.add_node(Masking(0.0),name='mask',input='input')
            model.add_node(TimeDistributedDense(HIDDEN_SIZE),name='transform',input='mas
            model.add_node(LSTM(HIDDEN_SIZE/4,return_sequences=True),name='forward',inpu
            model.add_node(LSTM(HIDDEN_SIZE/4),name='backward',input='forward')
            model.add_node(Dense(HIDDEN_SIZE, activation='sigmoid'),name='representation
            model.add_node(Dropout(0.5),name='dropout',input='representation')
            model.add_node(GaussianMixtureLayer(OUTPUT_SIZE),name='gauss',input='dropout
            model.add_node(Activation('softmax'),name='softmax',input='gauss')
            model.add_output(name='output',input='softmax')
            ```
    3. This model uses a lower dropout than the previous model
            ```
            model.add_input(name='input', input_shape=(None,FEATURE_DIM))
            model.add_node(Masking(0.0),name='mask',input='input')
            model.add_node(TimeDistributedDense(HIDDEN_SIZE),name='transform',input='mask')
            model.add_node(LSTM(HIDDEN_SIZE/8,return_sequences=True),name='forward',input='transform')
            model.add_node(LSTM(HIDDEN_SIZE/8),name='backward',input='forward')
            model.add_node(Dropout(0.3),name='dropout2',input='backward')
            model.add_node(Dense(HIDDEN_SIZE, activation='sigmoid'),name='representation',input='dropout2')
            model.add_node(Dropout(0.5),name='dropout',input='representation')
            model.add_node(GaussianMixtureLayer(OUTPUT_SIZE),name='gauss',input='dropout')
            model.add_node(Activation('softmax'),name='softmax',input='gauss')
            model.add_output(name='output',input='softmax')
            ```

## Results

The following results are all based on the development set of the BTAS2016 Challenge. Note that we always used PLP-39 Dimensional features as our input. When analyzing the results of the BLSTM and DNN we observed that the BLSTM only made mistakes for very short utterances with length <200 samples. While the DNN did overall made equally many mistakes for any utterance length, it did produce less mistakes on the utterances BLSTM struggled with. Thus we combined in our final models the features of DNN and BLSTM to obtain a robust feature.

Featuretype     | Model   | EER | Remark
---------:| :-----: |:-----: |  :----:
PLP-39-NoVAD  | GMM-UBM | 31.63% | 512 Component GMM
PLP-39-VAD | GMM-UBM | 4.83% | 512 Component GMM
PLP-39-VAD | LSTM | 0.76% |  1 LSTM, 2 hidden layers. Softmax output. 4 Outputneurons.
PLP-39-VAD | BLSTM | 0.54% |  1 LSTM, 2 hidden layers. Softmax output. 4 Outputneurons.
PLP-39-VAD | BLSTM;LDA | 0.46% |  1 LSTM, 2 hidden layers.
PLP-39-VAD | Multitask-DNN;LDA | 1.12% | PreLU Activation. (spk,attacktype,gender) as output. Mean reduce.
PLP-39-VAD | Multitask-DNN;LDA | 0.85% | ELU Activation. (spk,attacktype,gender) as output. Mean reduction.
PLP-39-VAD | DNN;LDA | 0.74% | RReLU Activation. attacktype (4 neurons) as output. Max reduction.
PLP-39-VAD | DNN;LDA | 0.64% | ELU Activation. Layer7, attacktype (4 neurons) as output. Mean reduce.
PLP-39-VAD | DNN;LDA | 0.56% | RReLU Activation. Dropout throughout. Layer4, attacktype (4 neurons) as output. Mean reduce.
PLP-39-VAD | DNN;LDA | 0.54% | ELU Activation. Dropout throughout. Layer3, attacktype (4 neurons) as output. Mean reduce.
PLP-39-VAD | CNN;LDA | 1.56% | PReLU Activation. 2 Conv. Layer4, attacktype (4 neurons) as output. Variance reduction
PLP-39-VAD | BLSTM;DNN;LDA;Fusion | 0.40% | Fusion of best vectors of BLSTM and ELU Activation. 1536 dimensional vectors
PLP-39-VAD | BLSTM;DNN;LDA;Fusion | **0.26%** | Fusion of best vectors of BLSTM (3 times 512) and ELU Activation. 2560 dimensional vectors
