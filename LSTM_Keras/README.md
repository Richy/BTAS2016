## Dependency

    Keras: https://github.com/fchollet/keras
    Numpy
    Scipy
    Scikit-learn

## Env

    source /speechlab/users/nxc04/env/bin/activate

## Training

    ./run.sh dnn44.py

## Decoding

    ./run.sh score.py lstm.pickle test.scp > test.txt

