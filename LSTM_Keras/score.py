from __future__ import print_function
import numpy as np
import cPickle
import sys
import os
from htk import HTKFeat_read
from sklearn.preprocessing import normalize
import theano
import theano.tensor as T
from keras.models import model_from_json
from keras import backend as K
def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)

last_file = None
data = None
def parse(fea_str):
    s = fea_str.rstrip().split('/')
    return s[-1], fea_str.rstrip()

def load_data(scp):
    global last_file
    for i in open(scp, "r"):
        name, file = parse(i)
        if file!=last_file:
            last_file = file
            data = HTKFeat_read(file).getall()
        yield name, np.array(data)

def get_output(fn, inputs, norm):
    #return fn.predict(np.array([norm.transform(inputs)]))
    return fn.predict({'input':np.array([norm.transform(inputs)])})['output'][0]
    #return fn([np.array([norm.transform(inputs)])])[0][0]

if len(sys.argv)!=3:
    print("Usage: ../run.sh write.py model scp")
    exit(0)
layer_idx=-14
with open(sys.argv[1], "rb") as f:
    norm = cPickle.load(f)
    model = cPickle.load(f)#.replace('"activation": "relu"','"activation": "linear"')
    #model = model_from_json(model)
    #model.load_weights('lstm.h5') 
#output=K.function([model.inputs[i].input for i in model.input_order],
#                      [model.nodes['representation'].get_output(train=False)])
for i in load_data(sys.argv[2]):
    name, value = i
    value = get_output(model, inputs=value, norm=norm)
    #value = get_output(model, inputs=value, norm=norm)
    print(name+" "+(" ".join(map(str, value))))
