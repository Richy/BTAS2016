import numpy as np
from htk import HTKFeat_read
import cPickle
data = None
LSTM_STEP = 50

def parse(fea_str, prefix):
    t = fea_str.rstrip()
    s = fea_str.rstrip().split('/')[-1]
    return prefix+"_"+s, t

def load_data(scp, mlf, prefix="train"):
    global data
    tr = {}
    for i in open(mlf,"r"):
        j = i.rstrip().split('\t')
        tr[prefix+"_"+j[0]] = int(j[1])
    X = []
    Y = []
    for i in open(scp,"r"):
        name, file = parse(i, prefix)
        #if not name in X.keys():
        #    X[name]=[]
        #    Y[name]=[]
        file_data=HTKFeat_read(file).getall()
        if file_data.shape[0]<LSTM_STEP:
            file_data=np.concatenate((file_data,np.zeros((LSTM_STEP-file_data.shape[0],file_data.shape[1]))),axis=0)
        if data is None:
            curr = 0
            data = file_data
        else:
            curr = data.shape[0]
            data = np.vstack((data, file_data))
        #print(str(curr)+":"+str(start)+","+str(end))
        #X.extend(range(curr, curr+end-start-LSTM_STEP+1))
        if prefix=="train":
            X.extend(range(curr, curr+file_data.shape[0]-LSTM_STEP+1))
            Y.extend([tr[name] for i in range(file_data.shape[0]-LSTM_STEP+1)])
            #print(str(X))
        else:
            X.append((curr, curr+file_data.shape[0]))
            Y.extend([tr[name]])
    #del file_data
    return X, Y

X_train, Y_train = load_data("train.scp", "mlf")
X_test, Y_test = load_data("dev.scp", "mlf","dev")
with open("spoof3.pickle", "wb") as f:
    ff = cPickle.Pickler(f,2)
    ff.dump(LSTM_STEP)
    ff.dump([data, X_train, Y_train, X_test, Y_test])

