#!/usr/bin/env python

import argparse as argp
from collections import defaultdict
import csv
from copy import copy

parser = argp.ArgumentParser()
parser.add_argument('inputscore',nargs='+',type=argp.FileType('r'),help="Input score files. ")
parser.add_argument('outputcsv',type=argp.FileType('wb'),help="Output file in csv format")

def scorefile(f):
    test_to_score=defaultdict(list)
    for line in f:
        line = line.rstrip('\n')
        enrolmdl,target_testutt,score = line.split()
        test_to_score[target_testutt].append(float(score))
    return test_to_score


def main():
    args = parser.parse_args()
    # use first scorefile as reference to get the keys
    ret=defaultdict(list)
    for sf in args.inputscore:
        for line in sf:
            line = line.rstrip('\n')
            enrolmdl,target_testutt,score = line.split()
            splits = target_testutt.split('-')
            testutt="".join(splits[1:])
            ret[testutt].append(float(score))
    writer = csv.writer(args.outputcsv,delimiter=' ')
    for key,scores in ret.iteritems():
        row = [key] + scores
        writer.writerow(row)

if __name__ == '__main__':
    main()
