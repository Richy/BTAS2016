#!/bin/bash
SCRIPTNAME="$(basename "$0")"
TARGETCLASSES=( 'genuine' 'attacks_voice' 'attacks_replay' 'attacks_speech' )
numargs=2
function usage() {
    echo "Usage:"
    echo "$SCRIPTNAME Filelist (Format is UTTERANCENAME) TARGETCLASS (${TARGETCLASSES[@]})"
    echo "Script produces an outputfile with TARGETMODEL ENROLMODEL-UTTERANCENAME format"
}

if [[ $# != $numargs ]]; then
    echo "Input: "$@
    echo "Error, need at least $numargs input arguments"
    usage
    exit
fi

array_contains () {
    local seeking=$1; shift
    local in=1
    for element; do
        if [[ $element == $seeking ]]; then
            in=0
            break
        fi
    done
    return $in
}

list=$1
targetclass=$2


array_contains $targetclass ${TARGETCLASSES[@]} || { echo "Targetclass must be one of the following: "${TARGETCLASSES[@]}; exit 1; }

# Check how many whitespaces we got
whitespaces=$(head -n 1 $list | tr -d -c ' ' | awk '{ print length; }')

if [[ $whitespaces > 1 ]]; then
    echo "Please verify that the format of the given file is only UTTERANCENAME (we found multiple other items per line)"
    exit
fi

# Output the produced TARGETCLASSES
awk -v var=$targetclass 'split($0,fname,"."){print var,var"-"fname[1];}' $list
