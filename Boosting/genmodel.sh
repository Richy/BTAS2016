#!/bin/bash
#Generates a file for the models, which then can be used to train any model
numargs=1
if [[ $# != $numargs ]]; then
    echo "Usage:"
    echo "$SCRIPTNAME FileList (Format is UTTERANCENAME)"
    echo "Script produces an outputfile with format (UTTERANCENAME LABELID)"
fi
list=$1

# Check how many whitespaces we got
whitespaces=$(head -n 1 $list | tr -d -c ' ' | awk '{ print length; }')

if [[ $whitespaces > 1 ]]; then
    echo "Please verify that the format of the given file is only UTTERANCENAME (we found multiple other items per line)"
    exit
fi

targetclasses=( 'genuine' 'attacks_voice' 'attacks_replay' 'attacks_speech' )

awk -v var="${targetclasses[*]}" 'BEGIN{split(var,list," ")}split($0,fname,"."){for (i in list){if ($0 ~ list[i]){print $0,list[i]; break;}}}' $list
