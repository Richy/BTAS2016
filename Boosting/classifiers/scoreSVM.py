#!/usr/bin/env python
import argparse as argp
import logging as log
import sys

import numpy as np
from sklearn.svm import SVC

import util

try:
    from sklearn.externals import joblib as pick
except ImportError as imp:
    import pickle as pick


extractors = {"cPickle": util.cPick, "txt": util.txtfile}

# A custom threshold for unseen utterances ( usually removed by VAD)
threshold = -100

parser = argp.ArgumentParser()
parser.add_argument("svmmodel", type=str,
                    help="Input trained SVM estimator")
parser.add_argument('testvectors', type=str,
                    help="The test utterances to score against")
parser.add_argument('testref', type=str,
                    help="Test references. A file containing the train and test models respectively")
parser.add_argument('-t', '--filetype', default="txt", choices=extractors,
                    help="The method how every line should be read, default is txt")
parser.add_argument('outputscore', type=argp.FileType(
    'w'), help="The output scores", default=sys.stdout)


def main():
    args = parser.parse_args()
    log.basicConfig(
        level=log.DEBUG, format='%(asctime)s %(levelname)s %(message)s', datefmt='%d/%m %H:%M:%S')
    log.info("Loading in SVM model from %s" % (args.svmmodel))
    svm = pick.load(args.svmmodel)
    encoder = svm.encoder
    log.info("Parsing labels and vectors")
    utttovector = util.parseutttomodelfile(
        args.testvectors, extractors[args.filetype])
    testreferences = util.readref(args.testref)
    log.info("Beginning testing")
    for enrolmodel, targets in testreferences.iteritems():
        try:
            enrolid = encoder.transform(enrolmodel)
        except ValueError as e:
            log.warn("Model %s not found in the training set" % (enrolmodel))
            continue
        for testutt, targetmdl in targets:
            if testutt not in utttovector:
                log.warn("Utterance %s not found in the testset" % (testutt))
                log.warn("Scoring utterance %s with threshold %i" %
                         (testutt, threshold))
                args.outputscore.write(
                    "{} {}-{} {:.3f}\n".format(enrolmodel, targetmdl, testutt, threshold))
                continue
            vector = np.array(utttovector[testutt])[np.newaxis, :]
            score = svm.predict_log_proba(vector)[0]
            score = score[enrolid]
            if np.isneginf(score):
                score = threshold

            args.outputscore.write(
                "{} {}-{} {:.3f}\n".format(enrolmodel, targetmdl, testutt, score))

    log.info("SVM estimation done, output file is: %s. Output file has the following structure: ENROLEMODEL TARGETMDL-TESTUTT" %
             (args.outputscore.name))
    # score = score[utttolabel]


if (__name__ == '__main__'):
    main()
