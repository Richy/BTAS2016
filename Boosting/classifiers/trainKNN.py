#!/usr/bin/env python
import argparse as argp
import logging as log

import numpy as np
import util
from sklearn.neighbors import KNeighborsClassifier

try:
    from sklearn.externals import joblib as pick
except ImportError as imp:
    import pickle as pick


extractors = {"cPickle": util.cPick, "txt": util.txtfile}

parser = argp.ArgumentParser()
parser.add_argument("trainfile", type=str,
                    help="Input training feature")
parser.add_argument('labelfile', type=str,help="Label file for the trainfeatures")
parser.add_argument('-t', '--filetype', default="txt", choices=extractors,
                    help="The method how every line should be read, default is txt")
parser.add_argument('-n', '--neighbors', type=int, default=5,
                    help='Number of neighbors considered in KNN')
parser.add_argument('outputmodel', type=str, help="The trained model")


def main():
    args = parser.parse_args()
    log.basicConfig(
        level=log.DEBUG, format='%(asctime)s %(levelname)s %(message)s', datefmt='%d/%m %H:%M:%S')
    log.info("Parsing labels and models")
    modeltovector = util.parseutttomodelfile(
        args.trainfile, extractors[args.filetype])
    utttolabel = util.parseutttolabel(args.labelfile)
    assert len(utttolabel)>0,"Couldnt find any utterance in the labelfile"
    models = np.empty((len(modeltovector.keys()), len(
        modeltovector.values()[0])), dtype='float')
    log.debug("Training vectors have size %s" % (models.shape,))
    labels = np.empty(len(modeltovector.keys()), dtype='uint')
    labelencoder = util.encodeLabels(utttolabel.values())
    for i, (utt, vec) in enumerate(modeltovector.iteritems()):
        models[i] = vec
        labels[i] = labelencoder.transform(utttolabel[utt])
    nn = KNeighborsClassifier(n_neighbors=args.neighbors)
    log.info("Fitting KNN model")
    nn.fit(models, labels)
    nn.encoder = labelencoder
    log.info("Fitting done, dumping model to %s" % (args.outputmodel))
    pick.dump(nn, args.outputmodel)


if (__name__ == '__main__'):
    main()
