from collections import defaultdict
import cPickle
import os
from sklearn import preprocessing

# reads the test_ref file
def readref(f):
    '''
    Testref needs to be in format TARGETMDL ENROLMODEL-TESTUTT
    '''
    tests = defaultdict(list)
    with open(f, 'r') as testpointer:
        for line in testpointer:
            line = line.rstrip('\n')
            targetmodel, enrol_testutt = line.split()[:2]
            modeltotestutt = enrol_testutt.split("-")
            enrolemdl = modeltotestutt[0]
            testutt = '-'.join(modeltotestutt[1:])
            tests[targetmodel].append((testutt, enrolemdl))
    return tests

def parseutttomodelfile(f,readfilefunc):
    modeltovector={}
    with open(f, 'r') as testpointer:
        for line in testpointer:
            line = line.rstrip('\n')
            modelname, vector = readfilefunc(line)
            modeltovector[modelname] = vector
    return modeltovector

# Readfilefunc for parseutttomodelfile
def cPick(uttname):
    return cPickle.load(uttname)

def txtfile(line):
    splits = line.split()
    # First item is the utteracename, the rest are floats
    return str(splits[0]),map(float, splits[1:])

def parseutttolabel(f):
    utttolabel = {}
    with open(f,'r') as lines:
        for line in lines:
            line = line.rstrip('\n')
            model,target = line.split()
            # Save non extension path
            utttolabel[os.path.splitext(model)[0]] = target
    return utttolabel

def encodeLabels(labels):
    le = preprocessing.LabelEncoder()
    le.fit(labels)
    return le

def decodelabels(encoder,values):
    return encoder.inverse_transform(values)

def unique(seq):
    uniqset = set(seq)
    return list(uniqset)
