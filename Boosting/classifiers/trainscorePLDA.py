#!/usr/bin/env python
import argparse as argp
import logging as log

import numpy as np

import util
from liblda import PLDA

try:
    from sklearn.externals import joblib as pick
except ImportError as imp:
    import pickle as pick


extractors = {"cPickle": util.cPick, "txt": util.txtfile}

parser = argp.ArgumentParser()
parser.add_argument("trainfile", type=str,
                    help="Input training features")
parser.add_argument('labelfile', type=str,
                    help="Label file for the trainfeatures")
parser.add_argument('testfile', type=str, help="Input test features")
parser.add_argument('testref', type=str, help="Testreference")
parser.add_argument('-iters', default=10, type=int,
                    help="Number of iterations for PLDA training")
parser.add_argument('-t', '--filetype', default="txt", choices=extractors,
                    help="The method how every line should be read, default is txt")
parser.add_argument('score', type=argp.FileType('w'), help="The output scores")


def main():
    args = parser.parse_args()
    log.basicConfig(
        level=log.DEBUG, format='%(asctime)s %(levelname)s %(message)s', datefmt='%d/%m %H:%M:%S')
    log.info("Parsing labels and models")
    modeltovector = util.parseutttomodelfile(
        args.trainfile, extractors[args.filetype])
    testtovector = util.parseutttomodelfile(
        args.testfile, extractors[args.filetype])
    utttolabel = util.parseutttolabel(args.labelfile)
    # Starting PLDA
    enrolvectors = np.empty((len(modeltovector.keys()), len(
        modeltovector.values()[0])), dtype='float')
    testvectors = np.empty((len(testtovector.keys()), len(
        testtovector.values()[0])), dtype='float')

    log.debug("Training vectors have size %s" % (enrolvectors.shape,))
    labels = np.empty(len(modeltovector.keys()), dtype='uint')
    testlabels = np.empty(len(testtovector.keys()), dtype='uint')
    enrolencoder = util.encodeLabels(utttolabel.values())
    testencoder = util.encodeLabels(testtovector.keys())

    for i, (utt, vec) in enumerate(modeltovector.iteritems()):
        enrolvectors[i] = vec
        labels[i] = enrolencoder.transform([utttolabel[utt]])
    for i, (utt, vec) in enumerate(testtovector.iteritems()):
        testvectors[i] = vec
        testlabels[i] = testencoder.transform([utt])

    plda = PLDA()
    log.info("Fitting PLDA model with %i iters" % (args.iters))
    plda.fit(enrolvectors, labels, args.iters)
    log.info("Fitting done, starting scoring")
    # Scoring
    testreferences = util.readref(args.testref)

    log.info("Transforming enrolment data vector to PLDA space")
    enroltransform = plda.transform(enrolvectors, labels)
    log.info("Transforming test data vector to PLDA space")
    testtransform = plda.transform(testvectors, testlabels)


    for enrolmodel, targets in testreferences.iteritems():
        try:
            enrolid = enrolencoder.transform(enrolmodel)
        except ValueError as e:
            log.warn("Model %s not found in the training set" % (enrolmodel))
            continue
        for testutt, targetmdl in targets:
            if testutt not in testtovector:
                log.warn("Utterance %s not found in the testset" % (testutt))
                log.warn("Scoring utterance %s with threshold %i" %
                         (testutt, threshold))
                args.outputscore.write(
                    "{} {}-{} {:.3f}\n".format(enrolmodel, targetmdl, testutt, threshold))
                continue

            enrolvec = enroltransform[enrolid]
            print(testtransform)
            print(testencoder.transform([testutt]))
            testvec = testtransform[testencoder.transform([testutt])]
            score = plda.score(enrolid, enrolvec, testvec)
            if np.isneginf(score):
                score = threshold

            args.outputscore.write(
                "{} {}-{} {:.3f}\n".format(enrolmodel, targetmdl, testutt, score))


if (__name__ == '__main__'):
    main()
