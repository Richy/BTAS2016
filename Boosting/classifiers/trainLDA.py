#!/usr/bin/env python
import argparse as argp
import logging as log

import numpy as np
try:
    from liblda import LDA
except ImportError:
    from sklearn.lda import LDA

import util

try:
    from sklearn.externals import joblib as pick
except ImportError as imp:
    import pickle as pick


extractors = {"cPickle": util.cPick, "txt": util.txtfile}

parser = argp.ArgumentParser()
parser.add_argument("trainfile", type=str,
                    help="Input training features")
parser.add_argument('labelfile', type=str,
                    help="Label file for the trainfeatures")
parser.add_argument('-t', '--filetype', default="txt", choices=extractors,
                    help="The method how every line should be read, default is txt")
parser.add_argument('outputmodel', type=str, help="The trained model")


def main():
    args = parser.parse_args()
    log.basicConfig(
        level=log.DEBUG, format='%(asctime)s %(levelname)s %(message)s', datefmt='%d/%m %H:%M:%S')
    log.info("Parsing labels and models")
    modeltovector = util.parseutttomodelfile(
        args.trainfile, extractors[args.filetype])
    utttolabel = util.parseutttolabel(args.labelfile)
    lda = LDA(solver='svd')
    models = np.empty((len(modeltovector.keys()), len(
        modeltovector.values()[0])), dtype='float')
    log.debug("Training vectors have size %s" % (models.shape,))
    labels = np.empty(len(modeltovector.keys()), dtype='uint')
    labelencoder = util.encodeLabels(utttolabel.values())
    for i, (utt, vec) in enumerate(modeltovector.iteritems()):
        models[i] = vec
        labels[i] = labelencoder.transform([utttolabel[utt]])
    log.info("Fitting LDA model")
    lda.fit(models, labels)
    lda.encoder = labelencoder
    log.info("Fitting done, dumping model to %s" % (args.outputmodel))
    pick.dump(lda, args.outputmodel)


if (__name__ == '__main__'):
    main()
