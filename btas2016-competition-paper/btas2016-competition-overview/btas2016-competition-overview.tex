\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{btas}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subfig}
\usepackage{hyperref}

\graphicspath{{figures/}}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
%\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}

%\btasfinalcopy % *** Uncomment this line for the final submission

\def\btasPaperID{****} % *** Enter the btas Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifbtasfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Overview of BTAS 2016 Speaker Anti-spoofing Competition}

%\author{Pavel Korshunov\\
%Biometrics group, Idiap Research Institute,\\
%Martigny, Switzerland\\
%{\tt\small pavel.korshunov@idiap.ch}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
%\and
%S\'{e}bastien Marcel\\
%Biometrics group, Idiap Research Institute,\\
%Martigny, Switzerland\\
%{\tt\small pavel.korshunov@idiap.ch}
%}

\maketitle
\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
This paper provides an overview of the speaker anti-spoofing competition organized by Biometric group at Idiap Research Institute for the IEEE Eighth International Conference on Biometrics: Theory, Applications, and Systems (BTAS 2016). The competition relied on a new speaker spoofing database AVspoof~\cite{Ergunay2015}, which provides a comprehensive variety of presentation attacks on the data from $44$ different speakers. It was demonstrated that the attacks contained in this database present an actual threat.

The paper states the competition goals, describes the database and the evaluation protocol, discusses solutions for speech spoofing detection submitted by the participants, and presents the results of the evaluation.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
Despite the growing usage and increasing reliability of the speaker verification systems, they are shown to be vulnerable to spoofing attacks. In a spoofing attack, an invalid user attempts to gain access to the system by presenting counterfeit (fake) speech sample(s) as the evidence of a valid user. Counterfeit speech can be synthesized from text, converted using speech of another person, or simply replayed using some playback device such as a mobile phone.

The participants in this anti-spoofing competition proposed spoofing attack detection techniques to protect an automatic speaker verification (ASV) system against spoofing attacks. Essentially, these techniques should effectively separate real (genuine) speech recordings from spoofed speech (attacks). 

A new speaker spoofing database AVspoof\footnote{https://www.idiap.ch/dataset/avspoof}~\cite{Ergunay2015} is used in the competition.  The database provides a comprehensive variety of realistic replay attacks via laptop speakers, high quality speakers, and two mobile phones for $44$ different speakers. Synthetic speech attacks, such as speech synthesis and voice conversion, are also included. It was demonstrated that the attacks contained in this database present an actual threat~\cite{Ergunay2015}. The participants were provided with two non-overlapping sets (each containing real and spoofed data subsets) for training and calibration of their spoofing attack detection techniques. The submitted techniques were evaluated on a separate independent testing set, which, besides the attacks present in AVspoof database, also included additional `unknown' attacks.

\begin{figure*}[tbh]
\centering
\subfloat{\includegraphics[width=0.32\textwidth]{figures/setup_session2_1}
  }
\subfloat{\includegraphics[width=0.141\textwidth]{figures/d3}
  }
\subfloat{\includegraphics[width=0.16\textwidth]{figures/mic}
  }
\subfloat{\includegraphics[width=0.32\textwidth]{figures/DSCN1119}
  }
\caption{AVspoof database recording setup.}
\label{fig:setup}
\end{figure*}


\section{Database}
The proposed competition is carried out using the AVspoof database\footnotemark[2]~\cite{Ergunay2015}. The database contains real (genuine) speech samples from $44$ participants ($31$ males and $13$ females) recorded over the period of two months in four sessions, each scheduled several days apart in different setups and environmental conditions such as background noises (Figure~\ref{fig:setup} demonstrates the recording setup). The first session was recorded in the most controlled conditions. Speech samples were recorded using three devices: laptop using microphone AT2020USB+ (referred to as `laptop'), Samsung Galaxy S4 phone (referred to as `phone1'), and iPhone 3GS  (referred to as `phone2'). With each device, the following types of speech were recorded:
%\begin{itemize}
%\item {Reading part (referred to as  `read'): $10$ or $40$ pre-defined sentences are read by participants.}
%\item {Pass-phrases part (referred to as  `pass'): $5$ short prompts are read by participants.}
%\item {Free speech part (referred to as `free'): Participants speak freely about any topic for $3$ to $10$ minutes.}
%\end{itemize}

Three major types of attacks are available in AVspoof database:
\begin{enumerate}
\item{{\bf replay attack}
Assuming that the original data (samples recorded by laptop) was stolen and is being replayed by some device with intention to spoof the verification system. Four devices were used to replay `pass' and `read' samples from all sessions: 
\begin{enumerate}
\item{laptop with internal speakers}
\item{laptop with high quality (HQ) speakers}
\item{phone1}
\item{phone2}
\end{enumerate}
}
\item{{\bf speech synthesis}
Assuming that laptop data from Session $1$ is stolen, they are used to synthesize `pass' and `read' samples for sessions $2-4$. Then, synthesized samples are played back to the verification system with the following:
\begin{enumerate}
\item{laptop with internal speakers}
\item{laptop with high quality (HQ) speakers}
\end{enumerate}
}
\item{{\bf voice conversion}
 Assuming that laptop data from Session $1$ is stolen, these samples are used by a voice conversion algorithm to generate `pass' and `read' samples for sessions $2-4$. Then, converted samples are played back to the verification system with the following:
\begin{enumerate}
\item{laptop with internal speakers}
\item{laptop with high quality (HQ) speakers}
\end{enumerate}
}
\end{enumerate}

\section{Evaluation protocol}
\label{sec:evaluation}

To have an unbiased evaluation, the samples of the database are split into three non-overlapping sets: training, development, and evaluation. Each set consists of two parts: (i) real or genuine data and (ii) spoofed data or attacks. 

Training and development sets was released to the participants at the start of the competition, so they had enough time to train their proposed spoofing attack detection systems on the training set and tune using the development set. The evaluation set was released at the end of the competition to assess and rank the accuracy of the proposed systems.

The evaluation of the spoofing attack detection system is done based on the \emph{false acceptance rate} (FAR) and the \emph{false rejection rate} (FRR).
The definition of these rates is dependent on a certain \emph{threshold} $\theta$:
\begin{equation}
  \label{eq:far-frr}
  \begin{split}
    \mathrm{FAR}(\theta) &= \frac{|\{h_{attack} \mid h_{attack} \geq \theta\}|}{|\{h_{attack}\}|}\\[.5ex]
    \mathrm{FRR}(\theta) &= \frac{|\{h_{real} \mid h_{real} < \theta\}|}{|\{h_{real}\}|}
  \end{split}
\end{equation}
where $h_{real}$ is a score for real or genuine data, while $h_{attack}$ is score for the attack or spoofed data.


We use the development set to define a score threshold $\theta_{dev}$, based on the \emph{Equal Error Rate} (EER) of the evaluated countermeasure. The final evaluation performance is then computed as the \emph{Half Total Error Rate} (HTER) (more details about the proposed evaluation can be found in~\cite{Chingovska2014}):
%\begin{equation}
%  \label{eq:eer-hter}
%  \begin{split}
%    \theta_{dev} = \argmin_\theta \frac{\mathrm{FAR}_{dev}(\theta) + \mathrm{FRR}_{dev}(\theta)}2\\[.5ex]
%    \mathrm{HTER}_{eval}(\theta_{dev}) = \frac{\mathrm{FAR}_{eval}(\theta_{dev}) + \mathrm{FRR}_{eval}(\theta_{dev})}2\\[.5ex]
%  \end{split}
%\end{equation}

The main goal for using the proposed evaluation protocol is to apply the same evaluation conditions to all participants. Such approach allows a fair and objective comparison between different submissions.


\begin{figure*}
\centering
\subfloat[DET curve, DEV set]{\includegraphics[width=0.45\textwidth]{temp_pad_det}
  }
\subfloat[Histogram distribution, DEV set]{\includegraphics[width=0.45\textwidth]{temp_pad_hist}
  }
\caption{A placeholder with the fake results. It will be replaced with the  results from the best submitted system.}
%\caption{The results of the best performing approach of the competition.}
\label{fig:hist}
\end{figure*}

\section{Baseline system}

A baseline spoofing attacks detection system was provided to participants to make it easier for them to create their own solution. The baseline system heavily relies on Bob toolbox~\cite{bob2012}\footnote{http://idiap.github.io/bob/}. The provided system reads audio files provided for the competition, extract simple spectrogram-based features, trains logistic regression classifier on the training set and, by using the trained classifier, computes score for the development set. From the resulted scores, EER value is computed and Detection Error Tradeoff (DET) curve is plotted. The baseline system results in EER value 5.91\% on the development set, so the goal of participants to develop a system that can beat this value and also perform well on the test set with included `unknown' attacks. 

\section{Submitted approaches}
Seven different teams from around the world registered for the competition and submitted their results for both development and test sets.

\input{sections/iitkgp_absp}
\input{sections/Hannah}
\input{sections/SJTUSpeech}
\input{sections/I2R}
\input{sections/CPqD}
\input{sections/plasmacoder}
\input{sections/TigerSpk}

\section{Evaluation of submissions}

\begin{table}[tbh]
\scriptsize
\centering
\caption{Placeholder for evaluation results table.}
\label{tab:results}

\begin{tabular}{l|c|c|c|c}

{\bf Team} & {\bf EER (Dev) } & {\bf FAR (Test) }  & {\bf FRR (Test) } & {\bf HTER (Test) } \\ \hline \hline
iitkgp\_absp & & & & \\ \hline 
SJTUSpeech & & & & \\ \hline 
I2R & & & & \\ \hline 
CPqD & & & & \\ \hline 
Hannah & & & & \\ \hline 
plasmacoder & & & & \\ \hline 
TigerSpk & & & & \\ \hline 

\end{tabular}
\end{table}

\section{Conclusion}

{\small
\bibliographystyle{ieee}
\bibliography{references}
}

\end{document}
